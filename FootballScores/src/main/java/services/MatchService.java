package services;

import domain.Match;
import exceptions.IncompleteObjectException;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import repository.MatchDAO;

import java.util.ArrayList;
import java.util.List;

public class MatchService {

    private MatchDAO dao;

    public MatchService(MatchDAO dao){
        this.dao = dao;
    }

    /**
     * Searches all existing matches in the database.
     * @return An ArrayList of Match objects.
     * @throws NoRecordsFoundException When the database returned an empty ResultSet
     */
    public List<Match> getAllMatches() throws NoRecordsFoundException {
        List<Match> matches = dao.getAllMatches();

        if(matches.isEmpty()){
            throw new NoRecordsFoundException();
        } else {
            return matches;
        }
    }

    /**
     * Searches for the specific match
     * @param id The ID of the match to search
     * @return A Match object
     * @throws InvalidIDException When the provided ID is 0 or smaller
     * @throws NoRecordsFoundException When the database returned an empty ResultSet
     */
    public Match getMatchById(int id) throws InvalidIDException, NoRecordsFoundException {
        if(id <= 0){
            throw new InvalidIDException();
        } else {
            Match match = dao.getMatchById(id);

            if(match == null){
                throw new NoRecordsFoundException();
            } else {
                return match;
            }
        }
    }

    /**
     * Searches matches ordered by latest first and returns as many as
     * required.
     * @param amount The amount of matches to be returned
     * @return an ArrayList of match objects
     * @throws NoRecordsFoundException When the database returned an empty ResultSet.
     */
    public List<Match> getLatestMatches(int amount) throws NoRecordsFoundException {
        if(amount <= 0){
            throw new IllegalArgumentException();
        } else {
            List<Match> matches = dao.getLatestMatches(amount);

            if(matches.isEmpty()){
                throw new NoRecordsFoundException();
            } else {
                return matches;
            }
        }
    }

    /**
     * Adds a new match to the matches table in the database.
     * @param match The match object containing the values required.
     * @return True if the match was added or false if no rows were inserted.
     * @throws IncompleteObjectException When the Match object does not contain all
     * of the required values.
     */
    public boolean addMatch(Match match) throws IncompleteObjectException {
        if(match.getHomeTeam() == null
        || match.getAwayTeam() == null
        || match.getMatchDay() == 0
        || match.getDivision().isEmpty()) throw new IncompleteObjectException();
        else {
            if(dao.addMatch(match)){
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Updates a match in the matches table in the database with the new values contained into the
     * provided Match object.
     * @param match The Match object containing the new values.
     * @param id The ID of the match to be updated.
     * @return True if the match was updated, false if the update failed.
     * @throws IncompleteObjectException If the match object does not contain all of the required values
     * @throws InvalidIDException If the ID is 0 or smaller.
     */
    public boolean editMatch(Match match, int id) throws IncompleteObjectException, InvalidIDException {
        if(match.getHomeTeam() == null
                || match.getAwayTeam() == null
                || match.getMatchDay() == 0
                || match.getDivision().isEmpty()){
            throw new IncompleteObjectException();
        } else {
            if(id <= 0){
                throw new InvalidIDException();
            } else {
                if(dao.editMatch(match, id)){
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Updates only the home team and away team scores of the match with the given ID.
     * @param homeTeamScore The new home team score value
     * @param awayTeamScore The new away team score value
     * @param id The ID of the match to be updated
     * @return True if the match was updated, false if the update failed
     * @throws InvalidIDException When the ID is 0 or smaller.
     */
    public boolean updateMatchScore(int homeTeamScore, int awayTeamScore, int id) throws InvalidIDException {
        if(homeTeamScore < 0 || awayTeamScore < 0){
            throw new IllegalArgumentException();
        } else {
            if(id <= 0){
                throw new InvalidIDException();
            } else{
                if(dao.updateMatchScore(homeTeamScore, awayTeamScore, id)){
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Deletes the given match from the database.
     * @param id The ID of the match to be deleted.
     * @return True if the match was deleted, false if the deletion failed.
     * @throws InvalidIDException If the ID is 0 or smaller.
     */
    public boolean deleteMatch(int id) throws InvalidIDException {
        if(id <= 0){
            throw new InvalidIDException();
        } else {
            if(dao.deleteMatch(id)){
                return true;
            } else {
                return false;
            }
        }
    }
}
