package services;

import domain.MatchEvent;
import exceptions.IncompleteObjectException;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import repository.MatchEventDAO;

import java.util.List;

public class MatchEventService {

    private MatchEventDAO dao;

    public MatchEventService(MatchEventDAO dao){
        this.dao = dao;
    }

    /**
     * Searches the database for all match events belonging to the given match.
     * @param matchId The ID of the match
     * @return An ArrayList of MatchEvent objects
     * @throws InvalidIDException When the provided ID is 0 or smaller.
     * @throws NoRecordsFoundException When an empty arraylist was returned, meaning no records were found in the database.
     */
    public List<MatchEvent> getMatchEventsFromMatch(int matchId) throws InvalidIDException, NoRecordsFoundException {
        if(matchId <= 0){
            throw new InvalidIDException();
        } else {
            List<MatchEvent> events = dao.getMatchEventsFromMatch(matchId);

            if(events.isEmpty()){
                throw new NoRecordsFoundException();
            } else {
                return events;
            }
        }
    }

    /**
     * Fetches a MatchEvent from the database corresponding to the given ID.
     * @param id The ID of the MatchEvent to find.
     * @return A MatchEvent object.
     * @throws InvalidIDException When the ID is 0 or smaller
     * @throws NoRecordsFoundException When the database returned an empty ResultSet.
     */
    public MatchEvent getMatchEventById(int id) throws InvalidIDException, NoRecordsFoundException {
        if(id <= 0){
            throw new InvalidIDException();
        } else {
            MatchEvent event = dao.getMatchEventById(id);

            if(event == null){
                throw new NoRecordsFoundException();
            } else {
                return event;
            }
        }
    }

    /**
     * Adds a new match event.
     * @param event The MatchEvent object containing the values to be inserted
     * @param matchId The ID of match this event belongs to
     * @return True if the event was added, false if no rows were added.
     * @throws IncompleteObjectException If not all the required values are contained in the MatchEvent object
     * @throws InvalidIDException If the matchId is 0 or smaller.s
     */
    public boolean addMatchEvent(MatchEvent event, int matchId) throws IncompleteObjectException, InvalidIDException {
        if(event != null){
            if(matchId <= 0){
                throw new InvalidIDException();
            } else {
                if(dao.addMatchEvent(event, matchId)){
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            throw new IncompleteObjectException();
        }
    }

    /**
     * Edits the match and updates the values to those in the MatchEvent object.
     * @param event The MatchEvent object containing the new values
     * @param eventId The ID of the MatchEvent to be updated
     * @return True if the MatchEvent was updated, false if no rows were updated.
     * @throws IncompleteObjectException If not all the required values are contained in the MatchEvent object
     * @throws InvalidIDException If the matchId is 0 or smaller.s
     */
    public boolean editMatchEvent(MatchEvent event, int eventId) throws IncompleteObjectException, InvalidIDException {
        if(event == null){
            throw new IncompleteObjectException();
        } else {
            if(eventId <= 0){
                throw new InvalidIDException();
            } else {
                if(dao.editMatchEvent(event, eventId)){
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Deletes the MatchEvent with the given ID from the match_events table in the database.
     * @param eventId The ID of the MatchEvent to be deleted.
     * @return True if the MatchEvent was deleted, false if no rows were deleted.
     * @throws InvalidIDException If the ID is 0 or smaller.
     */
    public boolean deleteMatchEvent(int eventId) throws InvalidIDException {
        if(eventId <= 0){
            throw new InvalidIDException();
        } else {
            if(dao.deleteMatchEvent(eventId)){
                return true;
            } else {
                return false;
            }
        }
    }
}
