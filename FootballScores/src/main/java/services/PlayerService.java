package services;

import domain.Player;
import exceptions.IncompleteObjectException;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import repository.PlayerDAO;

import java.sql.SQLException;
import java.util.List;

public class PlayerService {

    private PlayerDAO dao;

    public PlayerService(PlayerDAO dao){
        this.dao = dao;
    }

    /**
     * Searches the database for a player with the given ID.
     * @param id The id of the player to search for.
     * @return A Player object.
     * @throws InvalidIDException when the given id is 0 or smaller.
     * @throws NoRecordsFoundException when the player is null, meaning that no records were found.
     */
    public Player getPlayerById(int id) throws InvalidIDException, NoRecordsFoundException {
        if(id <= 0){
            throw new InvalidIDException();
        } else {
            Player player = dao.getPlayerById(id);

            if(player == null){
                throw new NoRecordsFoundException();
            } else {
                return player;
            }
        }
    }

    /**
     * Searches for the 5 players with the most goals scored and puts them in an ArrayList
     * ordered by the amount of goals they scored.
     * @return An ArrayList of Player objects
     * @throws NoRecordsFoundException When the database returned an empty ResultSet.
     */
    public List<Player> getPlayersWithMostGoals() throws NoRecordsFoundException {
        List<Player> players = dao.getPlayersWithMostGoals();
        if(players.isEmpty()){
            throw new NoRecordsFoundException();
        } else {
            return players;
        }
    }

    /**
     * Fetches all the players from the database and orders them by name alphabetically.
     * @return An ArrayList of players, sorted alphabetically.
     * @throws NoRecordsFoundException When the database returned an empty ResultSet.
     */
    public List<Player> getPlayers() throws NoRecordsFoundException {
        List<Player> players = dao.getPlayers();

        if(players.isEmpty()){
            throw new NoRecordsFoundException();
        } else {
            return players;
        }
    }

    /**
     * Fetches all the players playing for a team.
     * @param teamName The name of the team you want to show the players from.
     * @return An ArrayList of Player objects.
     * @throws IllegalArgumentException When the teamName provided is empty or is null.
     * @throws NoRecordsFoundException When the database returned an empty ResultSet.
     */
    public List<Player> getPlayersFromTeam(String teamName) throws NoRecordsFoundException {
        if(teamName.isEmpty() || teamName == ""){
            throw new IllegalArgumentException();
        } else {
            List<Player> players = dao.getPlayersFromTeam(teamName);

            if(players.isEmpty()){
                throw new NoRecordsFoundException();
            } else {
                return players;
            }
        }
    }

    /**
     * Adds the specified Player to the 'players' table in the database.
     * This method will return true if a row was inserted or false if no row was iserted, indicating
     * that something went wrong while executing the query.
     * @param player The player object to be added to the database.
     * @return True if the Player was succesfully added to the database.
     *         False if no rows were added to the database and the query most likely failed.
     * @throws IncompleteObjectException When the provided Player object is missing one or more of
     *                                   the following: String position, String name, String country,
     *                                   Date birthDate, int Team.id, int number
     */
    public boolean addPlayer(Player player) throws IncompleteObjectException {
        if(player.getPosition() != null
                && player.getName() != null
                && player.getCountry() != null
                && player.getBirthDate().toString() != null
                && player.getTeam() != null
                && player.getNumber() > 0){
            if(dao.addPlayer(player)){
                return true;
            } else {
                return false;
            }
        } else {
            throw new IncompleteObjectException();
        }
    }

    /**
     * Updates a player in the database and changes the contents to those of the provided
     * Player object. The player that will be updated in the database, is the one whose ID
     * matches with the provided ID as parameter.
     * @param player The player object containing the new values.
     * @param id The ID of the player in the database to be updated.
     * @return True if the update was successful or false if the update failed
     * @throws IncompleteObjectException If the Player object does not contain all of the required values
     * @throws InvalidIDException If the provided ID is 0 or smaller.
     */
    public boolean updatePlayer(Player player, int id) throws IncompleteObjectException, InvalidIDException {
        if(player.getPosition() != null
        && player.getName() != null
        && player.getCountry() != null
        && player.getBirthDate().toString() != null
        && player.getTeam() != null
        && player.getNumber() > 0){
            if(id <= 0){
                throw new InvalidIDException();
            } else {
                if(dao.updatePlayer(player, id)){
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            throw new IncompleteObjectException();
        }
    }

    /**
     * Deletes the player with the provided ID from the database.
     * @param id The ID of the player to be deleted.
     * @return True if the player was deleted from the database
     *         False if no rows were deleted.
     * @throws InvalidIDException If the provided ID is 0 or smaller.
     */
    public boolean deletePlayer(int id) throws InvalidIDException {
        if(id <= 0){
            throw new InvalidIDException();
        } else {
            if(dao.deletePlayer(id)){
                return true;
            } else {
                return false;
            }
        }
    }
}
