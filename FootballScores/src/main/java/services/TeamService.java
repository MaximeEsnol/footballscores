package services;

import domain.Team;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import repository.TeamDAO;

public class TeamService {

    private TeamDAO dao;

    public TeamService(TeamDAO dao){
        this.dao = dao;
    }

    /**
     * Searches the database for a team with the given ID.
     * @param id The ID of the team to search.
     * @return A Team object.
     * @throws InvalidIDException when the ID is 0 or smaller.
     * @throws NoRecordsFoundException when no records were found in the database and the returned Team object was null.
     */
    public Team getTeamById(int id) throws InvalidIDException, NoRecordsFoundException {
        if(id <= 0){
            throw new InvalidIDException();
        } else {
            Team team = dao.getTeamById(id);

            if(team == null){
                throw new NoRecordsFoundException();
            } else {
                return team;
            }
        }
    }
}
