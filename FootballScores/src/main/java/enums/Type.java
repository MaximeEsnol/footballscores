package enums;

public enum Type{
    GOAL,
    OWN_GOAL,
    YELLOW_CARD,
    RED_CARD,
    YELLOWRED_CARD,
    SUBSTITUTION,
    PENALTY_MISSED,
    PENALTY_SCORED,
    GOAL_CANCELLED
}
