package application;


import com.sun.media.jfxmedia.events.PlayerStateEvent;
import domain.MatchEvent;
import domain.Player;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import repository.MatchDAO;
import repository.MatchEventDAO;
import repository.PlayerDAO;
import repository.TeamDAO;
import services.MatchEventService;
import services.MatchService;
import services.PlayerService;
import services.TeamService;
import utilities.ConnectionManager;

import java.sql.Connection;

public class StartApp {
    public static void main(String[] args) {
        MatchDAO matchDAO = new MatchDAO();
        MatchService matchService = new MatchService(matchDAO);

        MatchEventDAO matchEventDAO = new MatchEventDAO();
        MatchEventService matchEventService = new MatchEventService(matchEventDAO);

        PlayerDAO playerDAO = new PlayerDAO();
        PlayerService playerService = new PlayerService(playerDAO);

        TeamDAO teamDAO = new TeamDAO();
        TeamService teamService = new TeamService(teamDAO);
    
        Runner runner = new Runner(matchService, matchEventService, playerService, teamService);
        runner.startApp();
    }
}
