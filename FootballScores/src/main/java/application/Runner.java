package application;

import domain.Match;
import domain.MatchEvent;
import domain.Player;
import domain.Team;
import enums.Side;
import enums.Type;
import exceptions.IncompleteObjectException;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import services.MatchEventService;
import services.MatchService;
import services.PlayerService;
import services.TeamService;
import utilities.Input;

import java.beans.SimpleBeanInfo;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import utilities.Formatter;

import static utilities.Formatter.*;

public class Runner {

    private static Scanner sc = new Scanner(System.in);
    private static Input input = new Input(sc);

    private MatchService matchService;
    private MatchEventService matchEventService;
    private PlayerService playerService;
    private TeamService teamService;
    private boolean continueRunning;

    public Runner(MatchService matchService, MatchEventService matchEventService, PlayerService playerService, TeamService teamService){
        this.matchService = matchService;
        this.matchEventService = matchEventService;
        this.playerService = playerService;
        this.teamService = teamService;

        continueRunning = true;
    }

    /**
     * Starts the app by showing a welcome message.
     */
    public void startApp(){
        System.out.println("                        ██╗    ██╗███████╗██╗      ██████╗ ██████╗ ███╗   ███╗███████╗    ██████╗                   \n" +
                "                        ██║    ██║██╔════╝██║     ██╔════╝██╔═══██╗████╗ ████║██╔════╝    ╚════██╗                  \n" +
                "                        ██║ █╗ ██║█████╗  ██║     ██║     ██║   ██║██╔████╔██║█████╗       █████╔╝                  \n" +
                "                        ██║███╗██║██╔══╝  ██║     ██║     ██║   ██║██║╚██╔╝██║██╔══╝      ██╔═══╝                   \n" +
                "                        ╚███╔███╔╝███████╗███████╗╚██████╗╚██████╔╝██║ ╚═╝ ██║███████╗    ███████╗                  \n" +
                "                         ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝    ╚══════╝                  \n" +
                "                                                                                                                    \n" +
                "███████╗ ██████╗  ██████╗ ████████╗██████╗  █████╗ ██╗     ██╗     ███████╗ ██████╗ ██████╗ ██████╗ ███████╗███████╗\n" +
                "██╔════╝██╔═══██╗██╔═══██╗╚══██╔══╝██╔══██╗██╔══██╗██║     ██║     ██╔════╝██╔════╝██╔═══██╗██╔══██╗██╔════╝██╔════╝\n" +
                "█████╗  ██║   ██║██║   ██║   ██║   ██████╔╝███████║██║     ██║     ███████╗██║     ██║   ██║██████╔╝█████╗  ███████╗\n" +
                "██╔══╝  ██║   ██║██║   ██║   ██║   ██╔══██╗██╔══██║██║     ██║     ╚════██║██║     ██║   ██║██╔══██╗██╔══╝  ╚════██║\n" +
                "██║     ╚██████╔╝╚██████╔╝   ██║   ██████╔╝██║  ██║███████╗███████╗███████║╚██████╗╚██████╔╝██║  ██║███████╗███████║\n" +
                "╚═╝      ╚═════╝  ╚═════╝    ╚═╝   ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝\n" +
                "                                                                                                                    ");


        showMainMenu();
    }

    /**
     * Shows the program's main menu.
     */
    private void showMainMenu() {
        System.out.println("========== MAIN MENU ==========");
        System.out.println("[1] VIEW MATCHES");
        System.out.println("[2] VIEW PLAYERS");
        System.out.println("[3] ADD OR EDIT PLAYERS");
        System.out.println("[4] ADD OR EDIT MATCHES");
        System.out.println("[5] QUIT");
        System.out.println("===============================");

        int option = input.getInt(true);

        executeMainMenuOption(option);
    }

    /**
     * Executes the option chosen by the user in the main menu.
     * @param option The option number matching an option in the main menu.
     */
    private void executeMainMenuOption(int option) {
        switch(option){
            case 1:
                showMatchMenu();
                break;
            case 2:
                showPlayerMenu();
                break;
            case 3:
                showAddEditPlayersMenu();
                break;
            case 4:
                showAddEditMatchMenu();
                break;
            case 5:
                continueRunning = false;
                break;
            default:
                sc.nextLine();
                showMainMenu();
                break;
        }

        if(continueRunning){
            showMainMenu();
        }
    }

    /**
     * Shows the menu to add, edit or delete matches and match events.
     */
    private void showAddEditMatchMenu() {
        boolean editMenuRepeat = true;
        System.out.println("---------- ADD/EDIT MATCHES AND MATCH EVENTS ----------");
        System.out.println("<=====> MENU <=====>");
        System.out.println("[1] MATCHES");
        System.out.println("[2] MATCH EVENTS");
        System.out.println("[3] MAIN MENU");

        int id = input.getInt(true);
        executeAddEditMatchMenuOption(id, editMenuRepeat);
    }

    private void executeAddEditMatchMenuOption(int id, boolean editMenuRepeat) {
        switch(id){
            case 1:
                showMatchAdminMenu();
                break;
            case 2:
                showMatchEventAdminMenu();
                break;
            case 3:
                editMenuRepeat = false;
                break;
            default:
                showAddEditMatchMenu();
                break;
        }

        if(editMenuRepeat){
            showAddEditMatchMenu();
        }
    }

    /**
     * Shows the menu for match event administration.
     */
    private void showMatchEventAdminMenu() {
        boolean repeat = true;
        System.out.println("|---| MATCH EVENT ADMINISTRATION |---|");
        System.out.println("[1] ADD NEW MATCH EVENT");
        System.out.println("[2] EDIT EXISTING MATCH EVENT");
        System.out.println("[3] DELETE MATCH EVENT");
        System.out.println("[4] GO BACK");
        
        int id = input.getInt(true);
        
        executeMatchEventMenu(id, repeat);
    }

    /**
     * Executes the option selected by the user in the match event administration menu.
     * @param id The ID of the option that was chosen.
     * @param repeat Whether or not to repeat the menu after one of the actions have been executed.
     */
    private void executeMatchEventMenu(int id, boolean repeat) {
        switch(id){
            case 1:
                startAddMatchEvent();
                break;
            case 2:
                startEditMatchEvent();
                break;
            case 3:
                startDeleteMatchEvent();
                break;
            case 4:
                repeat = false;
                break;
            default:
                showMatchEventAdminMenu();
                break;
        }

        if(repeat){
            showMatchEventAdminMenu();
        }
    }

    /**
     * Shows the menu for match administration.
     */
    private void showMatchAdminMenu() {
        boolean repeat = true;
        System.out.println("|---| MATCH ADMINISTRATION |---|");
        System.out.println("[1] ADD NEW MATCH");
        System.out.println("[2] EDIT EXISTING MATCH");
        System.out.println("[3] EDIT EXISTING MATCH SCORE");
        System.out.println("[4] DELETE A MATCH");
        System.out.println("[5] GO BACK");

        int id = input.getInt(true);

        executeMatchAdminMenu(id, repeat);
    }

    private void executeMatchAdminMenu(int id, boolean repeat) {
        switch(id){
            case 1:
                startAddMatch();
                break;
            case 2:
                startEditMatch();
                break;
            case 3:
                startEditMatchScore();
                break;
            case 4:
                startDeleteMatch();
                break;
            case 5:
                repeat = false;
                break;
            default:
                showMatchAdminMenu();
                break;
        }

        if(repeat){
            showMatchAdminMenu();
        }
    }

    /**
     * Shows the menu to add or edit player information.
     */
    private void showAddEditPlayersMenu() {
        boolean editMenuRepeat = true;
        System.out.println("---------- ADD/EDIT PLAYERS ----------");
        System.out.println("<=====> MENU <=====>");
        System.out.println("[1] ADD NEW PLAYER");
        System.out.println("[2] EDIT EXISTING PLAYER");
        System.out.println("[3] DELETE A PLAYER");
        System.out.println("[4] MAIN MENU");

        int i = input.getInt(true);
        executeAddEditPlayerOption(i, editMenuRepeat);
    }

    /**
     * Executes the selected option from the Add/Edit player menu.
     * @param i The option to be executed
     * @param editMenuRepeat True if the Add/Edit player menu has to be shown again afterwards.
     *                       False if the main menu should be shown afterwards.
     */
    private void executeAddEditPlayerOption(int i, boolean editMenuRepeat) {
        switch (i){
            case 1:
                startAddPlayer();
                break;
            case 2:
                startEditPlayer();
                break;
            case 3:
                startDeletePlayer();
                break;
            case 4:
                editMenuRepeat = false;
                break;
            default:
                showAddEditPlayersMenu();
                break;
        }

        if(editMenuRepeat){
            showAddEditPlayersMenu();
        }
    }

    /**
     * Shows the menu with player options
     */
    private void showPlayerMenu() {
        boolean playerMenuRepeat = true;
        System.out.println("---------- PLAYERS ----------");
        System.out.println("<=====> MOST GOALS <=====>");

        try {
            List<Player> players = playerService.getPlayersWithMostGoals();
            System.out.println("ID\t\tPlayer\t\t\t\t\tGls");
            for(Player p : players){
                System.out.println(p.getId() + "\t\t" + p.getName() + "\t\t\t" + p.getExtraData());
            }
        } catch (NoRecordsFoundException e) {
            e.printStackTrace();
        }

        showPlayerMenuOptions(playerMenuRepeat);
    }

    /**
     * Shows the menu with options about players.
     * @param playerMenuRepeat True if the menu should show again after executing one of the options, false if
     *                         it should navigate back to the main menu afterwards.
     */
    private void showPlayerMenuOptions(boolean playerMenuRepeat) {
        System.out.println("<=====> PLAYER MENU <=====>");
        System.out.println("[1] VIEW ALL PLAYERS");
        System.out.println("[2] VIEW PLAYERS FROM TEAM");
        System.out.println("[3] VIEW PLAYER DETAILS");
        System.out.println("[4] MAIN MENU");

        int i = input.getInt(true);
        executePlayerMenuOption(i, playerMenuRepeat);
    }

    private void executePlayerMenuOption(int i, boolean playerMenuRepeat) {
        switch(i){
            case 1:
                showAllPlayers();
                break;
            case 2:
                showSpecificTeamPlayers();
                break;
            case 3:
                askThenShowPlayerDetails();
                break;
            case 4:
                playerMenuRepeat = false;
                break;
            default:
                showPlayerMenuOptions(playerMenuRepeat);
                break;

        }

        if(playerMenuRepeat){
            showPlayerMenuOptions(playerMenuRepeat);
        }
    }




    /**
     * Shows the menu with latest matches.
     */
    private  void showMatchMenu() {
        boolean matchMenuRepeat = true;
        System.out.println("---------- MATCHES ----------");
        System.out.println("<=====> LATEST <=====>");

        try {
            List<Match> matches = matchService.getLatestMatches(5);

            for(Match m : matches){
                System.out.println(formatMatchShort(m));
            }
        } catch (NoRecordsFoundException e) {
            e.printStackTrace();
        }

        showMatchMenuOptions(matchMenuRepeat);
    }

    /**
     * Shows the menu for the Matches
     */
    private void showMatchMenuOptions(boolean repeat){
        System.out.println("<=====> MATCH MENU <=====>");
        System.out.println("[1] LIST MATCHES");
        System.out.println("[2] VIEW MATCH (by ID)");
        System.out.println("[3] MAIN MENU");
        int i = input.getInt(true);
        executeMatchMenuOption(i, repeat);
    }

    /**
     * Executes an option from the Match Menu
     * @param id The ID of the option to execute
     * @param repeat If true the menu will be repeated after the option was executed, if false the main menu will be shown
     */
    private void executeMatchMenuOption(int id, boolean repeat){
        switch(id){
            case 1:
                showAllMatches();
                break;
            case 2:
                showSpecificMatch();
                break;
            case 3:
                repeat = false;
                break;
            default:
                showMatchMenuOptions(repeat);
                break;
        }

        if(repeat){
            showMatchMenuOptions(repeat);
        }
    }

    /**
     * Asks the user to insert the ID of the match to show.
     */
    private void showSpecificMatch() {
        System.out.println("----- MATCH FINDER -----");
        int id = input.getInt(true);

        showMatchDetails(id);
    }

    /**
     * Shows an interface with match details and match events about the chosen match
     * @param id The ID of the match to show details from
     */
    private void showMatchDetails(int id) {
        try {
            Match match = matchService.getMatchById(id);

            System.out.println("============================");
            System.out.println(formatMatchShort(match));

            List<MatchEvent> events = matchEventService.getMatchEventsFromMatch(match.getId());

            for(MatchEvent e : events){
                System.out.println(formatMatchEventInMatch(e));
            }

            System.out.println("============================");
        } catch (InvalidIDException e) {
            System.out.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Shows a list of all the matches ever played.
     */
    private void showAllMatches(){
        try {
            List<Match> matches = matchService.getAllMatches();

            for(Match m : matches){
                System.out.println(formatMatchShort(m));
            }

        } catch (NoRecordsFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Shows all the players registered in the database.
     */
    private void showAllPlayers() {
        try {
            List<Player> players = playerService.getPlayers();

            for(Player p : players){
                System.out.println(formatPlayerShort(p));
            }
        } catch (NoRecordsFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Asks the user to insert the name of the team to show players from.
     * Then, will shows all the players playing for the given team.
     */
    private void showSpecificTeamPlayers() {
        System.out.println("----- Team name: -----");
        String teamName = input.getString(true);

        showPlayersFromTeam(teamName);
    }

    /**
     * Shows all the players belonging to the provided team.
     * @param teamName The name of the team to show the players from.
     */
    private void showPlayersFromTeam(String teamName) {
        try {
            List<Player> players = playerService.getPlayersFromTeam(teamName);

            for(Player p : players){
                System.out.println(formatPlayerShort(p));
            }
        } catch (NoRecordsFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Asks the user to insert the ID of a player, then shows all the details about
     * the provided player.
     */
    private void askThenShowPlayerDetails() {
        System.out.println("----- Player ID: -----");
        int id = input.getInt(true);

        showPlayerDetails(id);
    }

    /**
     * Shows details about the player with the given ID.
     * @param id The ID of the player to show.
     */
    private void showPlayerDetails(int id) {
        try {
            Player player = playerService.getPlayerById(id);

            System.out.println(Formatter.formatPlayerLong(player));
        } catch (InvalidIDException e) {
            System.out.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * Shows the interface to delete a player.
     * The user is asked to enter the ID of the player to delete. The inserted ID is checked
     * to be sure a player exists. If a player exists, the user is asked for
     * confirmation whether or not to delete this player. After the confirmation is received
     * the player gets deleted entirely from the database.
     */
    private void startDeletePlayer() {
        System.out.println("------------------------------");
        System.out.println("-      Delete a  player      -");
        System.out.println("------------------------------");

        System.out.println("ID of the player to delete: ");
        int id = input.getInt(true);

        try {
            Player player = playerService.getPlayerById(id);

            System.out.println("Are you sure you want to delete " + player.getName().toUpperCase() + "? Y/N: ");
            String choice = input.getString(true).toLowerCase();

            if(choice.equals("y")){
                playerService.deletePlayer(id);
            } else {
                System.out.println("Cancelled. " + player.getName().toUpperCase() + " has not been deleted.");
            }
        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Shows the interface to edit a player.
     */
    private void startEditPlayer() {
        System.out.println("------------------------------");
        System.out.println("-        Edit a player       -");
        System.out.println("------------------------------");

        System.out.println("ID of the player to edit: ");
        int id = input.getInt(true);

        try {
            Player player = playerService.getPlayerById(id);

            System.out.println("FULL NAME (" + player.getName() + "): ");
            String name = input.getString(true);

            System.out.println("COUNTRY OF ORIGIN (" + player.getCountry() + "): ");
            String country = input.getString(true);

            System.out.println("BIRTH DATE (" + new SimpleDateFormat("yyyy-MM-dd").format(player.getBirthDate()) + "): ");
            Date birthDate = input.getDate(true);

            System.out.println("PREFERRED PLAYING POSITION (" + player.getPosition() + "): ");
            String position = input.getString(true);

            System.out.println("SHIRT NUMBER (" + player.getNumber() + "): ");
            int number = input.getInt(true);

            System.out.println("TEAM ID (" + player.getTeam().getId() + "): ");
            int teamId = input.getInt(true);

            Player updatedPlayer = createPlayerObject(name, country, position, number, teamId, birthDate);

            updatePlayerInDatabase(updatedPlayer, id);
        } catch (InvalidIDException e) {
            System.out.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * Shows the interface to add a player
     */
    private void startAddPlayer() {
        System.out.println("------------------------------");
        System.out.println("-      Add a new player      -");
        System.out.println("------------------------------");

        String  name,
                country,
                position;
        int     number,
                teamId;
        Date    birthDate;

        System.out.println("FULL NAME: ");
        name = input.getString(true);

        System.out.println("COUNTRY OF ORIGIN: ");
        country = input.getString(true);
        System.out.println("BIRTH DATE: ");
        birthDate = input.getDate(true);
        System.out.println("PREFERRED PLAYING POSITION: ");
        position = input.getString(true);
        System.out.println("SHIRT NUMBER: ");
        number = input.getInt(true);
        System.out.println("TEAM ID: ");
        teamId = input.getInt(true);

        Player player = createPlayerObject(name, country, position, number, teamId, birthDate);

        addPlayerToDatabase(player);
    }

    /**
     * Creates the player object based on the provided variables.
     * @param name The name of the player
     * @param country The country of origin of the player
     * @param position The preferred playing position of the player
     * @param number The shirt number of the player
     * @param teamId The team ID the players plays for
     * @param birthDate The birthdate of the player.
     * @return A player object that can be inserted into the database
     */
    private Player createPlayerObject(String name, String country, String position, int number, int teamId, Date birthDate) {
        Player player = new Player();
        player.setName(name);
        player.setCountry(country);
        player.setPosition(position);
        player.setNumber(number);
        player.setBirthDate(birthDate);
        Team team = new Team();
        team.setId(teamId);
        player.setTeam(team);

        return player;
    }

    /**
     * Adds the player object to the database.
     * @param player the player to be added in the database.
     */
    private void addPlayerToDatabase(Player player) {
        try {
            if(playerService.addPlayer(player)){
                System.out.println("<=> " + player.getName() + " was added to the database! <=> ");
            } else {
                System.out.println("/!\\ Oops! " + player.getName() + " could not be added to the database!");
            }
        } catch (IncompleteObjectException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Updates the player with the given ID in the database with the values of the
     * updatedPlayer parameter.
     * @param updatedPlayer The Player object containing the new values.
     * @param id The ID of the player in the database to be updated.
     */
    private void updatePlayerInDatabase(Player updatedPlayer, int id) {
        try{
            if(playerService.updatePlayer(updatedPlayer, id)){
                System.out.println("<=> " + updatedPlayer.getName() + " was updated in the database! <=> ");
            } else {
                System.out.println("/!\\ Oops! " + updatedPlayer.getName() + " could not be updated in the database!");
            }
        } catch (IncompleteObjectException e) {
            System.err.println(e.getMessage());
        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Shows the interface to add a new match
     */
    private void startAddMatch() {
        System.out.println("------------------------------");
        System.out.println("-      Add a  new match      -");
        System.out.println("------------------------------");

        int homeTeam,
            awayTeam,
            matchDay,
            homeTeamScore,
            awayTeamScore;
        String division;

        System.out.println("HOME TEAM ID: ");
        homeTeam = input.getInt(true);

        System.out.println("AWAY TEAM ID: ");
        awayTeam = input.getInt(true);

        System.out.println("MATCH DAY: ");
        matchDay = input.getInt(true);

        System.out.println("DIVISION: ");
        division = input.getString(true);

        System.out.println("HOME TEAM SCORE: ");
        homeTeamScore = input.getInt(true);

        System.out.println("AWAY TEAM SCORE: ");
        awayTeamScore = input.getInt(true);

        Match match = createMatchObject(homeTeam, awayTeam, matchDay, division, homeTeamScore, awayTeamScore);

        addMatchToDatabase(match);
    }

    /**
     * Shows the interface to edit a match.
     */
    private void startEditMatch() {
        System.out.println("------------------------------");
        System.out.println("-        Edit a match        -");
        System.out.println("------------------------------");

        int homeTeam,
                awayTeam,
                matchDay,
                homeTeamScore,
                awayTeamScore;
        String division;

        System.out.println("ID of the match to edit: ");
        int id = input.getInt(true);

        try {
            Match m = matchService.getMatchById(id);

            System.out.println("HOME TEAM ID ("+m.getHomeTeam().getId()+"): ");
            homeTeam = input.getInt(true);

            System.out.println("AWAY TEAM ID ("+m.getAwayTeam().getId() + "): ");
            awayTeam = input.getInt(true);

            System.out.println("MATCH DAY ("+m.getMatchDay() + "): ");
            matchDay = input.getInt(true);

            System.out.println("DIVISION ("+m.getDivision()+"): ");
            division = input.getString(true);

            System.out.println("HOME TEAM SCORE ("+m.getHomeTeamScore()+"): ");
            homeTeamScore = input.getInt(true);

            System.out.println("AWAY TEAM SCORE ("+m.getAwayTeamScore()+"): ");
            awayTeamScore = input.getInt(true);

            Match match = createMatchObject(homeTeam, awayTeam, matchDay, division, homeTeamScore, awayTeamScore);

            updateMatchInDatabase(match, id);

        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Shows the interface to edit match scores
     */
    private void startEditMatchScore() {
        System.out.println("------------------------------");
        System.out.println("-    Edit a match  scores    -");
        System.out.println("------------------------------");

        int homeTeamScore,
            awayTeamScore,
            matchId;

        System.out.println("ID of the match to edit: ");
        matchId = input.getInt(true);

        try {
            Match m = matchService.getMatchById(matchId);

            System.out.println("HOME TEAM SCORE (" + m.getHomeTeamScore() + "):");
            homeTeamScore = input.getInt(true);

            System.out.println("AWAY TEAM SCORE (" + m.getAwayTeamScore() + "):");
            awayTeamScore = input.getInt(true);

            if(matchService.updateMatchScore(homeTeamScore, awayTeamScore, matchId)){
                System.out.println("<==> The scores have been updated! <==>");
            } else {
                System.out.println("/!\\ Oops! Could not update the scores.");
            }
        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Shows the interface to delete a match from the database.
     */
    private void startDeleteMatch() {
        System.out.println("------------------------------");
        System.out.println("-       Delete a match       -");
        System.out.println("------------------------------");

        System.out.println("ID of the match to delete: ");
        int matchId = input.getInt(true);

        try {
            Match m = matchService.getMatchById(matchId);

            System.out.println("You are about to delete: " + m.getHomeTeam().getName() + " - " + m.getAwayTeam().getName() + "! Are you sure? Y/N: ");
            String choice = input.getString(true).toLowerCase();

            if(choice.equals("y")){
                if(matchService.deleteMatch(matchId)){
                    System.out.println("<==> The match " + m.getHomeTeam().getName() + " - " + m.getAwayTeam().getName() + " was succesfully deleted! <==>");
                } else {
                    System.out.println("/!\\ Oops! Could not delete this match.");
                }
            } else{
                System.out.println("Cancelled. The match " + m.getHomeTeam().getName() + " - " + m.getAwayTeam().getName() + " was not deleted.");
            }
        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.err.println(e.getMessage());
        }
    }



    /**
     * Updates an entry of the matches table in the database with the new values provided.
     * @param match The Match object containing the new values
     * @param id The ID of the match to be updated.s
     */
    private void updateMatchInDatabase(Match match, int id) {
        try {
            if(matchService.editMatch(match, id)){
                System.out.println("<==> The match was succesfully edited! <==>");
            } else {
                System.out.println("/!\\ Oops! Could not edit the match.");
            }
        } catch (IncompleteObjectException e) {
            System.err.println(e.getMessage());
        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Shows the interface to create a new Match Event
     */
    private void startAddMatchEvent() {
        System.out.println("------------------------------");
        System.out.println("-    Create a Match Event    -");
        System.out.println("------------------------------");

        int     matchId,
                time,
                extraTime,
                playerId;

        String  type,
                side;

        System.out.println("MATCH ID: ");
        matchId = input.getInt(true);

        System.out.println("TIME OF THE EVENT: ");
        time = input.getInt(true);

        System.out.println("EXTRA TIME: ");
        extraTime = input.getInt(true);

        System.out.println("AFFECTED PLAYER ID: ");
        playerId = input.getInt(true);

        System.out.println("EVENT TYPE: ");

        System.out.println("Choose one of the following and type it down as such: ");
        for(Type t : Type.values()){
            System.out.println(t);
        }
        type = matchEventChooseType();

        System.out.println("EVENT SIDE: ");
        System.out.println("Choose one of the following and type it down as such: ");
        for(Side s : Side.values()){
            System.out.println(s);
        }
        side = matchEventChooseSide();

        MatchEvent event = createMatchEventObject(time, extraTime, playerId, type, side);

        createMatchEventInDatabase(event, matchId);
    }

    /**
     * Shows the interface to edit an existing match event.
     */
    private void startEditMatchEvent() {
        System.out.println("------------------------------");
        System.out.println("-     Edit a Match Event     -");
        System.out.println("------------------------------");

        System.out.println("ID of the match event to edit: ");
        int eventId = input.getInt(true);

        try {
            MatchEvent e = matchEventService.getMatchEventById(eventId);

            int     matchId,
                    time,
                    extraTime,
                    playerId;

            String  type,
                    side;


            System.out.println("(i): You can't edit the match ID of an event. To do so, you'll have to delete the current Event and" +
                    "create a new one.");
            System.out.println("TIME OF THE EVENT (" + e.getTime() + "): ");
            time = input.getInt(true);

            System.out.println("EXTRA TIME (" + e.getExtraTime() + "): ");
            extraTime = input.getInt(true);

            System.out.println("AFFECTED PLAYER ID (" + e.getMainPlayer().getId() + "): ");
            playerId = input.getInt(true);

            System.out.println("EVENT TYPE (" + e.getType() + "): ");

            System.out.println("Choose one of the following and type it down as such: ");
            for(Type t : Type.values()){
                System.out.println(t);
            }
            type = matchEventChooseType();

            System.out.println("EVENT SIDE (" + e.getSide() + "): ");
            System.out.println("Choose one of the following and type it down as such: ");
            for(Side s : Side.values()){
                System.out.println(s);
            }
            side = matchEventChooseSide();

            MatchEvent event = createMatchEventObject(time, extraTime, playerId, type, side);

            updateMatchEventInDatabase(event, eventId);
        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Shows the interface to delete a MatchEvent from the database.
     */
    private void startDeleteMatchEvent() {
        System.out.println("------------------------------");
        System.out.println("-    Delete a Match Event    -");
        System.out.println("------------------------------");

        System.out.println("ID of the match event to delete: ");
        int id = input.getInt(true);

        try {
            MatchEvent event = matchEventService.getMatchEventById(id);

            System.out.println("Are you sure you want to delete MatchEvent: " + event.getType() + " from " + event.getMainPlayer().getName() + "? Y/N: ");
            String choice = input.getString(true).toUpperCase();

            if(choice.equals("Y")){
                if(matchEventService.deleteMatchEvent(id)){
                    System.out.println("<==> The event has been deleted successfully! <==>");
                } else {
                    System.out.println("/!\\ Oops! Could not delete this match event.");
                }
            } else {
                System.out.println("Cancelled. The event "+ event.getType() + " from " + event.getMainPlayer().getName() + " has not been deleted.");
            }
        } catch (InvalidIDException e) {
            System.err.println(e.getMessage());
        } catch (NoRecordsFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Updates an existing Match Event in the database. The event that will be updated
     * is the one with the id of the provided eventId.
     * @param event The new values to be applied to existing MatchEvent
     * @param eventId The ID of the MatchEvent to be updated.
     */
    private void updateMatchEventInDatabase(MatchEvent event, int eventId) {
        try {
            if(matchEventService.editMatchEvent(event, eventId)){
                System.out.println("<==> The event was updated successfully! <==>");
            } else {
                System.out.println("/!\\ Oops! Could not update this match event.");
            }
        } catch (IncompleteObjectException e) {
            System.out.println(e.getMessage());
        } catch (InvalidIDException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Creates a new match event in the database and shows whether or not the
     * query was executed succesfully.
     * @param event The MatchEvent object that will be used to create the new entry
     * @param matchId The ID of the match this event applies to
     */
    private void createMatchEventInDatabase(MatchEvent event, int matchId) {
        try {
            if(matchEventService.addMatchEvent(event, matchId)){
                System.out.println("<==> The event was created successfully! <==>");
            } else {
                System.out.println("/!\\ Oops! Could not create a new match event.");
            }
        } catch (IncompleteObjectException e) {
            System.out.println(e.getMessage());
        } catch (InvalidIDException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Creates a new MatchEvent object that can be used to insert a new entry in the
     * match_events table in the database.
     * @param time The time at which the event takes place
     * @param extraTime The time during the stoppage time at which the event takes place
     * @param playerId The player ID who created or is the reason for the event
     * @param type The type of event
     * @param side The side for which this event applies to.
     * @return A MatchEvent object which can be used in the database.
     */
    private MatchEvent createMatchEventObject(int time, int extraTime, int playerId, String type, String side) {
        MatchEvent event = new MatchEvent();
        event.setTime(time);
        event.setExtraTime(extraTime);
        event.setType(type);
        event.setSide(side);

        Player player = new Player();
        player.setId(playerId);
        event.setMainPlayer(player);

        return event;
    }

    /**
     * Iterates through the Side enumeration to verify if the input is also
     * a value that exists in it.
     * @return The input of the user if it appears in the Side enum.
     */
    private String matchEventChooseSide() {
        String side = input.getString(true).toUpperCase();

        for(Side s : Side.values()){
            if(s.equals(side))
                return side;
        }

        matchEventChooseType();
        return null;
    }

    /**
     * Iterates through the Type enumeration to verify if the input is also
     * a value that exists in it.
     * @return The input of the user if it appears in the Type enum.
     */
    private String matchEventChooseType() {
        String type = input.getString(true).toUpperCase();

        for(Type t : Type.values()){
            if(t.equals(type))
                return type;
        }
        matchEventChooseType();
        return null;
    }


    /**
     * Adds the provided Match object to the database.
     * @param match The match object to be added to the database.
     */
    private void addMatchToDatabase(Match match) {
        try {
            if(matchService.addMatch(match)){
                System.out.println("<==> The match was added succesfully to the database! <==>");
            } else {
                System.out.println("/!\\ Oops! Could not add the match to the database.");
            }
        } catch (IncompleteObjectException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Creates a Match object with the minimal required values for the matches table in the database.
     * @param homeTeam The ID of the home team
     * @param awayTeam The ID of the away team
     * @param matchDay The number of the match day
     * @param division The division in which the game is played
     * @param homeTeamScore The score for the home team at full time
     * @param awayTeamScore The score for the away team at full time
     */
    private Match createMatchObject(int homeTeam, int awayTeam, int matchDay, String division, int homeTeamScore, int awayTeamScore) {
        Match match = new Match();
        match.setHomeTeamScore(homeTeamScore);
        match.setAwayTeamScore(awayTeamScore);
        match.setMatchDay(matchDay);
        match.setDivision(division);

        Team home = new Team();
        home.setId(homeTeam);
        match.setHomeTeam(home);

        Team away = new Team();
        away.setId(awayTeam);
        match.setAwayTeam(away);

        return match;
    }

}
