package domain;

import java.util.Date;

public class Player {

    private int id;
    private String name;
    private Team team;
    private Date birthDate;
    private String position;
    private String country;
    private int number;
    private String extraData;

    public Player(){};

    /**
     * Creates a Player object. A Player plays for a Team. The Player object contains information
     * about the Player such as the name, team the player plays for, birthdaye, position, country of origin
     * and the shirt number of the player.
     * @param id The ID of the player.
     * @param name The full name of the player with the first name first and last name last.
     * @param team The Team the player currently plays for.
     * @param birthDate The date on which the player was born.
     * @param position The position the player prefers playing as.
     * @param country The country of origin of the player. If the player plays for a different nation
     *                than their country of origin, then the nation the player plays for is used.
     * @param number The shirt number of the player at their current team.
     */
    public Player(int id, String name, Team team, Date birthDate, String position, String country, int number) {
        this.id = id;
        this.name = name;
        this.team = team;
        this.position = position;
        this.country = country;
        this.number = number;
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", team=" + team +
                ", birthDate=" + birthDate +
                ", position='" + position + '\'' +
                ", country='" + country + '\'' +
                ", number=" + number +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getBirthDate(){
        return birthDate;
    }

    public void setBirthDate(Date birthDate){
        this.birthDate = birthDate;
    }

    public void setExtraData(String extraData){
        this.extraData = extraData;
    }

    public String getExtraData(){
        return extraData;
    }
}
