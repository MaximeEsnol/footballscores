package domain;

import enums.Side;
import enums.Type;

public class MatchEvent {

    private int id;
    private int time;
    private int extraTime;
    private Type type;
    private Player mainPlayer;
    private Side side;

    public MatchEvent(){};

    /**
     * Creates a new MatchEvent object with the given parameters.
     * A MatchEvent contains more detailed information about what events occurred during
     * a Match.
     * @param id The ID of the MatchEvent
     * @param time The time at which the event occurred. It is an integer between 1 and 120.
     * @param extraTime The extra time at which the event occurred. For example if 4 minutes of time were added at
     *                  the end of a Match, and a player scored a goal in the 90+3 minute. The time parameter would be
     *                  90 and the extra time parameter would be 3. If there was no extra time, just use 0.
     * @param type The type of this event. This is a Type enumeration, for example GOAL, YELLOW_CARD,...
     * @param mainPlayer The player who is responsible or who is affected by the event. For example,
     *                   the player who scored the goal, or the player who received a yellow card.
     * @param side The side for which this event applies. For example if the home team scored a goal, then Side
     *             would be HOME_TEAM. If the event is for the away team, then it would be AWAY_TEAM.
     */
    public MatchEvent(int id, int time, int extraTime, Type type, Player mainPlayer, Side side) {
        this.id = id;
        this.time = time;
        this.extraTime = extraTime;
        this.type = type;
        this.mainPlayer = mainPlayer;
        this.side = side;
    }

    @Override
    public String toString() {
        return "MatchEvent{" +
                "id=" + id +
                ", time=" + time +
                ", extraTime=" + extraTime +
                ", type=" + type +
                ", mainPlayer=" + mainPlayer +
                ", side=" + side +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getExtraTime() {
        return extraTime;
    }

    public void setExtraTime(int extraTime) {
        this.extraTime = extraTime;
    }

    public String getType() {
        return this.type.name();
    }

    public void setType(String type) {
        this.type = Type.valueOf(type);
    }

    public Player getMainPlayer() {
        return mainPlayer;
    }

    public void setMainPlayer(Player mainPlayer) {
        this.mainPlayer = mainPlayer;
    }

    public String getSide() {
        return this.side.name();
    }

    public void setSide(String side) {
        this.side = Side.valueOf(side);
    }

}
