package domain;

import java.util.ArrayList;

public class Match {

    private int id;
    private Team homeTeam;
    private Team awayTeam;
    private String division;
    private int matchDay;
    private int homeTeamScore;
    private int awayTeamScore;

    public Match(){
        this.id = 0;
        this.homeTeam = null;
        this.awayTeam = null;
        this.division = null;
        this.matchDay = 0;
        this.homeTeamScore = 0;
        this.awayTeamScore = 0;
    };

    /**
     * Creates a new Match object. A Match is when two different Teams play each other. Match objects contain
     * information about the full time score, which teams played each other, in which division the game took place and
     * on what Match day this happened.
     * @param id The ID of the match
     * @param homeTeam The home Team or the team who is considered the home team.
     * @param awayTeam The away Team or the team who is considered the away team.
     * @param division The division in which the game took place. For example Premier League, Pro League 1A,...
     * @param matchDay The match day at which the game took place.
     * @param homeTeamScore The score for the home Team at full time.
     * @param awayTeamScore The score for the away Team at full time.
     */
    public Match(int id, Team homeTeam, Team awayTeam, String division, int matchDay, int homeTeamScore, int awayTeamScore) {
        this.id = id;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.division = division;
        this.matchDay = matchDay;
        this.homeTeamScore = homeTeamScore;
        this.awayTeamScore = awayTeamScore;
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", homeTeam=" + homeTeam +
                ", awayTeam=" + awayTeam +
                ", division='" + division + '\'' +
                ", matchDay=" + matchDay +
                ", homeTeamScore=" + homeTeamScore +
                ", awayTeamScore=" + awayTeamScore +
                '}';
    }

    public int getHomeTeamScore() {
        return homeTeamScore;
    }

    public void setHomeTeamScore(int homeTeamScore) {
        this.homeTeamScore = homeTeamScore;
    }

    public int getAwayTeamScore() {
        return awayTeamScore;
    }

    public void setAwayTeamScore(int awayTeamScore) {
        this.awayTeamScore = awayTeamScore;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public int getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(int matchDay) {
        this.matchDay = matchDay;
    }


}
