package utilities;

import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    private DateUtils(){};

    /**
     * Calculates the amount of years between the date provided and the current date.
     * @param date The birth date.
     * @return The difference in years between both dates.
     */
    public static int getAge(Date date){
        if(date.after(new Date())){
            throw new DateTimeException("The provided date is not before the current date.");
        } else {
            Calendar birth = Calendar.getInstance();
            birth.setTime(date);

            Calendar now = Calendar.getInstance();
            now.setTime(new Date());

            int age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
            if(birth.get(Calendar.DAY_OF_YEAR) > now.get(Calendar.DAY_OF_YEAR)){
                age--;
            }

            return age;
        }
    }
}
