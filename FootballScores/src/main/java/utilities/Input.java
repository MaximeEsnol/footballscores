package utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Input {

    private Scanner sc;

    public Input(Scanner sc){
        this.sc = sc;
    }

    /**
     * Asks the user to insert a number, when an integer is needed from
     * the input stream.
     * @param tryAgainIfFailed Whether or not the method should be ran again if an incorrect value is provided.
     * @return The integer that the user provided
     */
    public int getInt(boolean tryAgainIfFailed){
        System.out.print("Insert a round number: ");

        if(sc.hasNextInt()) {
            return Integer.parseInt(sc.nextLine());
        } else {
            if(tryAgainIfFailed){
                sc.nextLine();
                getInt(true);
            } else {
                return 0;
            }
        }
        return 0;
    }

    /**
     * Asks the user to insert a comma number, when a double is needed from the
     * input stream.
     * @param tryAgainIfFailed Whether or not the method should be ran again if an incorrect value is provided.
     * @return The double that the user provided
     */
    public double getDouble(boolean tryAgainIfFailed){
        System.out.print("Insert a comma number: ");

        if(sc.hasNextDouble()){
            return Double.parseDouble(sc.nextLine());
        } else {
            if(tryAgainIfFailed){
                sc.nextLine();
                getDouble(true);
            } else {
                return 0;
            }
        }
        return 0;
    }

    /**
     * Asks the user to insert a String of text.
     * @param tryAgainIfFailed Whether or not the methoud should be ran again if an incorrect value is provided.
     * @return The String that the user provided
     */
    public String getString(boolean tryAgainIfFailed){
        System.out.print("Insert text: ");
        if(sc.hasNextLine()){
            return sc.nextLine();
        } else {
            if(tryAgainIfFailed){
                sc.nextLine();
                getString(true);
            } else {
                return null;
            }
        }
        return null; 
    }

    /**
     * Asks the user to insert a date in the following format: YYYY-MM-DD.
     * @param tryAgainIfFailed Whether or not the methoud should be ran again if an incorrect value is provided.
     * @return The Date the user inserted as a Date object.
     */
    public Date getDate(boolean tryAgainIfFailed){
        System.out.print("Insert a date (YYYY-MM-DD): ");
        if(sc.hasNextLine()){
            try {
                return new SimpleDateFormat("yyyy-MM-dd").parse(sc.nextLine());
            } catch (ParseException e) {
                System.err.println("The date provided is in the incorrect format.");
                if(tryAgainIfFailed) {
                    sc.nextLine();
                    getDate(true);
                }
            }
        } else {
            sc.nextLine();
            getDate(true);
        }
        return null;
    }
}
