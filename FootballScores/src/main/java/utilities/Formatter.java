package utilities;

import domain.Match;
import domain.MatchEvent;
import domain.Player;
import enums.Side;

public class Formatter {

    private Formatter(){}

    /**
     * Formats the match information in short so the match ID, teams playing each other and the score can be seen easily.
     * @param match The match object to be formatted into a String
     * @return The formatted String
     */
    public static String formatMatchShort(Match match){
        StringBuilder builder = new StringBuilder();

        builder.append("[" + match.getId() + "]");
        builder.append("\t\t");
        builder.append(match.getHomeTeam().getName());
        builder.append("   " + match.getHomeTeamScore() + " - " + match.getAwayTeamScore() + "   ");
        builder.append("\t\t");
        builder.append(match.getAwayTeam().getName());

        return builder.toString();
    }

    /**
     * Formats the match event information so the details about the match are displayed clearly and
     * in a readable way.
     * @param event The MatchEvent object to be formatted
     * @return A formatted String of the MatchEvent
     */
    public static String formatMatchEventInMatch(MatchEvent event){
        StringBuilder builder = new StringBuilder();

        if(event.getSide() == "HOME_TEAM"){
            builder.append("[" + event.getType() + "] ");
            builder.append("("+ event.getTime() + ((event.getExtraTime() > 0) ? "+" + event.getExtraTime() : "") + "') ");
            builder.append(event.getMainPlayer().getName());
        } else if(event.getSide() == "AWAY_TEAM"){
            builder.append("\t\t\t\t\t\t\t\t\t");
            builder.append(event.getMainPlayer().getName());
            builder.append("("+ event.getTime() + ((event.getExtraTime() > 0) ? "+" + event.getExtraTime() : "") + "') ");
            builder.append("[" + event.getType() + "] ");
        }

        return builder.toString();
    }

    /**
     * Formats the player information such as the ID, name and team the player plays for so that it can be
     * displayed cleanly and in a readable way in the console.
     * @param player The player object that has to be formatted
     * @return A formatted string of the Player object.
     */
    public static String formatPlayerShort(Player player){
        StringBuilder builder = new StringBuilder();

        builder.append("["+ player.getId() +"]");
        builder.append("    " + player.getName());
        builder.append("  ("+ player.getTeam().getName() +")");

        return builder.toString();
    }

    public static String formatPlayerLong(Player player){
        StringBuilder builder = new StringBuilder();

        builder.append("            " + player.getName());
        builder.append("\nTEAM: \t\t\t" + player.getTeam().getName() + "("+ player.getTeam().getCountry() +", "+ player.getTeam().getDivision() +")");
        builder.append("\nNUMBER: \t\t" + player.getNumber() + "  POSITION: " + player.getPosition());
        builder.append("\nAGE: " + DateUtils.getAge(player.getBirthDate()));

        return builder.toString();
    }
}
