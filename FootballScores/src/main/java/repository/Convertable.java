package repository;

import java.sql.ResultSet;

public interface Convertable {

    /**
     * Creates an Object based on the provided resultset.
     * @param rs The ResultSet containing the data for the object to create.
     * @return An object based on the provided resultset.
     */
    Object sqlToObject(ResultSet rs);

}
