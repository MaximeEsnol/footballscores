package repository;

import domain.Match;
import domain.Team;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import services.TeamService;
import utilities.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MatchDAO implements Convertable{

    private Connection con;

    public MatchDAO(){
        try {
            this.con = DriverManager.getConnection(ConnectionManager.URL, ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected MatchDAO(Connection con){
        this.con = con;
    }

    /**
     * Searches all existing matches in the database.
     * @return An ArrayList of Match objects.
     */
    public List<Match> getAllMatches(){
        List<Match> matches = new ArrayList<>();

        String query = "SELECT * " +
                "       FROM matches m " +
                "       LEFT JOIN teams ht ON ht.id = m.home_team" +
                "       LEFT JOIN teams at ON at.id = m.away_team";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.execute();

            ResultSet rs = s.getResultSet();

            while(rs.next()){
                Match match = (Match)sqlToObject(rs);
                matches.add(match);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return matches;
    }

    /**
     * Searches matches ordered by latest first and returns as many as
     * required.
     * @param amount The amount of matches to be returned
     * @return an ArrayList of match objects
     */
    public List<Match> getLatestMatches(int amount){
        List<Match> matches = new ArrayList<>();
        String query = "SELECT * FROM matches m " +
                "       LEFT JOIN teams ht ON ht.id = m.home_team" +
                "       LEFT JOIN teams at ON at.id = m.away_team" +
                "       ORDER BY m.id DESC LIMIT " + amount;

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.execute();

            ResultSet rs = s.getResultSet();
            while(rs.next()){
                Match match = (Match)sqlToObject(rs);
                matches.add(match);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return matches;
    }

    /**
     * Searches for the specific match
     * @param id The ID of the match to search
     * @return A Match object
     */
    public Match getMatchById(int id){
        String query = "SELECT * FROM matches m " +
                "       LEFT JOIN teams ht ON ht.id = m.home_team" +
                "       LEFT JOIN teams at ON at.id = m.away_team" +
                "       WHERE m.id = ? ";
        Match match = null;

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, id);
            s.execute();

            ResultSet rs = s.getResultSet();

            if(rs.next()){
                match = (Match)sqlToObject(rs);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return match;
    }

    /**
     * Adds a new match into the database based on the Match object provided.
     * @param match The match object based on which a record has to be created in the database
     * @return True if a new row was added
     *         False if no record was added.
     */
    public boolean addMatch(Match match){
        int homeTeam = match.getHomeTeam().getId();
        int awayTeam = match.getAwayTeam().getId();
        String division = match.getDivision();
        int matchDay = match.getMatchDay();
        int homeTeamScore = match.getHomeTeamScore();
        int awayTeamScore = match.getAwayTeamScore();

        String query = "INSERT INTO matches(home_team, away_team, division, match_day, home_team_score, away_team_score)" +
                "       VALUES (?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, homeTeam);
            s.setInt(2, awayTeam);
            s.setString(3, division);
            s.setInt(4, matchDay);
            s.setInt(5, homeTeamScore);
            s.setInt(6, awayTeamScore);

            if(s.executeUpdate() > 0){
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    return false;
    }

    /**
     * Updates the match with the given ID in the database and changes its values to the ones from the
     * Match object.
     * @param match The match object containing the new values
     * @param id The ID of the database row to be updated
     * @return True if the row was updated or false if no rows were updated.
     */
    public boolean editMatch(Match match, int id){
        int homeTeam = match.getHomeTeam().getId();
        int awayTeam = match.getAwayTeam().getId();
        String division = match.getDivision();
        int matchDay = match.getMatchDay();
        int homeTeamScore = match.getHomeTeamScore();
        int awayTeamScore = match.getAwayTeamScore();

        String query = "UPDATE matches SET home_team = ?," +
                "                          away_team = ?," +
                "                          division = ?," +
                "                          match_day = ?," +
                "                          home_team_score = ?," +
                "                          away_team_score = ?" +
                "       WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, homeTeam);
            s.setInt(2, awayTeam);
            s.setString(3, division);
            s.setInt(4, matchDay);
            s.setInt(5, homeTeamScore);
            s.setInt(6, awayTeamScore);
            s.setInt(7, id);

            if(s.executeUpdate() > 0){
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Deletes a match from the database.
     * @param id The ID of the database row to be deleted from the matches table.
     * @return True if the match was deleted or false if no match was deleted.
     */
    public boolean deleteMatch(int id){
        String query = "DELETE FROM matches WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, id);

            if(s.executeUpdate() > 0){
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Updates only the home team score and away team score from the given match.
     * @param homeTeamScore The new home team score
     * @param awayTeamScore The new away team score
     * @param id The ID of match to be updated
     * @return True if the match was updated, false if no rows were updated.
     */
    public boolean updateMatchScore(int homeTeamScore, int awayTeamScore, int id){
        String query = "UPDATE matches SET home_team_score = ? AND away_team_score = ? WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, homeTeamScore);
            s.setInt(2, awayTeamScore);
            s.setInt(3, id);

            if(s.executeUpdate() > 0){
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Creates a Match object from the given ResultSet. Additionally, two Team objects will be created
     * as they are necessary in the Match object. However, no ArrayList of MatchEvent objects will be
     * created.
     *
     * @param rs The ResultSet containing the data for the object to create.
     * @return A Match object
     */
    @Override
    public Object sqlToObject(ResultSet rs) {
        Match match = new Match();

        try {
            match.setId(rs.getInt("m.id"));
            match.setDivision(rs.getString("m.division"));
            match.setMatchDay(rs.getInt("m.match_day"));
            match.setHomeTeamScore(rs.getInt("m.home_team_score"));
            match.setAwayTeamScore(rs.getInt("m.away_team_score"));

            if(rs.getInt("m.home_team") > 0){
                Team team = new Team();
                team.setId(rs.getInt("ht.id"));
                team.setName(rs.getString("ht.name"));
                team.setDivision(rs.getString("ht.division"));
                team.setCountry(rs.getString("ht.country"));
                match.setHomeTeam(team);
            } else {
                match.setHomeTeam(null);
            }

            if(rs.getInt("m.away_team") > 0){
                Team team = new Team();
                team.setId(rs.getInt("at.id"));
                team.setName(rs.getString("at.name"));
                team.setDivision(rs.getString("at.division"));
                team.setCountry(rs.getString("at.country"));

                match.setAwayTeam(team);
            } else {
                match.setAwayTeam(null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return match;
    }
}
