package repository;

import domain.Team;
import utilities.ConnectionManager;

import java.sql.*;

public class TeamDAO implements Convertable {

    private Connection con;

    public TeamDAO(){
        try {
            this.con = DriverManager.getConnection(ConnectionManager.URL, ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected TeamDAO(Connection con){
        this.con = con;
    }

    /**
     * Searches the database for a team with the given ID.
     * @param id The ID of the team to search.
     * @return A Team object.
     */
    public Team getTeamById(int id){
        Team team = null;
        String query = "SELECT * FROM teams WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, id);
            s.execute();

            ResultSet rs = s.getResultSet();

            if(rs.next()){
                team = (Team)sqlToObject(rs);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return team;
    }

    /**
     * Creates an Object based on the provided resultset.
     *
     * @param rs The ResultSet containing the data for the object to create.
     * @return An object based on the provided resultset.
     */
    @Override
    public Object sqlToObject(ResultSet rs) {
        Team team = new Team();
        try {
            team.setId(rs.getInt("id"));
            team.setCountry(rs.getString("country"));
            team.setDivision(rs.getString("division"));
            team.setName(rs.getString("name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return team;
    }
}
