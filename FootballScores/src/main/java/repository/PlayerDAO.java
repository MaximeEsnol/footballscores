package repository;

import domain.Player;
import domain.Team;
import utilities.ConnectionManager;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PlayerDAO implements Convertable {

    private Connection con;

    public PlayerDAO() {
        try {
            this.con = DriverManager.getConnection(ConnectionManager.URL, ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected PlayerDAO(Connection con) {
        this.con = con;
    }

    /**
     * Searches the database for a player with the given id.
     *
     * @param id The id of the player to search for.
     * @return A Player object.
     */
    public Player getPlayerById(int id) {
        Player player = null;
        String query = "SELECT * FROM players p" +
                "       LEFT JOIN teams t ON p.team_id = t.id" +
                "       WHERE p.id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, id);
            s.execute();

            ResultSet rs = s.getResultSet();

            if (rs.next()) {
                player = (Player) sqlToObject(rs);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return player;
    }

    /**
     * Searches for the 5 players with the most goals scored and puts them in an ArrayList
     * ordered by the amount of goals they scored.
     *
     * @return An ArrayList of Player objects
     */
    public List<Player> getPlayersWithMostGoals() {
        List<Player> players = new ArrayList<>();
        String query = "SELECT " +
                " p.id " +
                " ,p.name " +
                " ,p.team_id" +
                " ,p.birth_date " +
                " ,p.position " +
                " ,p.country " +
                " ,p.number " +
                " ,t.id " +
                " ,t.name " +
                " ,t.country " +
                " ,t.division" +
                " ,COUNT(me.main_player) AS goalsScored FROM players p " +
                "LEFT JOIN teams t ON t.id = p.team_id " +
                "LEFT JOIN match_events me ON me.main_player = p.id " +
                "WHERE type IN ('GOAL', 'PENALTY_SCORED') " +
                "GROUP BY me.main_player " +
                "ORDER BY goalsScored DESC LIMIT 5";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.execute();

            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                Player p = (Player) sqlToObject(rs);
                p.setExtraData(rs.getString("goalsScored"));
                players.add(p);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return players;
    }

    /**
     * Fetches all the players from the database and orders them by name alphabetically.
     *
     * @return An ArrayList of players, sorted alphabetically.
     */
    public List<Player> getPlayers() {
        List<Player> players = new ArrayList<>();
        String query = "SELECT * FROM players p " +
                "       LEFT JOIN teams t ON t.id = p.team_id " +
                "       ORDER BY p.name ASC";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.execute();

            ResultSet rs = s.getResultSet();

            while (rs.next()) {
                Player p = (Player) sqlToObject(rs);
                players.add(p);
            }
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return players;
    }

    /**
     * Fetches all the players playing for a team.
     *
     * @param teamName The name of the team you want to show the players from.
     * @return An ArrayList of Player objects.
     */
    public List<Player> getPlayersFromTeam(String teamName) {
        List<Player> players = new ArrayList<>();
        String query = "SELECT * FROM players p " +
                "       LEFT JOIN teams t ON p.team_id = t.id " +
                "       WHERE t.name = ? " +
                "       ORDER BY p.name ASC, p.number ASC";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setString(1, teamName);
            s.execute();

            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                Player player = (Player) sqlToObject(rs);
                players.add(player);
            }
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return players;
    }

    /**
     * Adds the specified Player to the 'players' table in the database.
     * This method will return true if a row was inserted or false if no row was iserted, indicating
     * that something went wrong while executing the query.
     *
     * @param player The player object to be added to the database.
     * @return True if the Player was succesfully added to the database.
     * False if no rows were added to the database and the query most likely failed.
     */
    public boolean addPlayer(Player player) {
        String birthDate = new SimpleDateFormat("yyyy-MM-dd").format(player.getBirthDate());
        String country = player.getCountry();
        String name = player.getName();
        int number = player.getNumber();
        String position = player.getPosition();
        int team = player.getTeam().getId();

        String query = "INSERT INTO players (name, team_id, birth_date, position, country, number) VALUES (?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setString(1, name);
            s.setInt(2, team);
            s.setString(3, birthDate);
            s.setString(4, position);
            s.setString(5, country);
            s.setInt(6, number);

            if (s.executeUpdate() > 0) {
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Updates a player in the database and changes the contents to those of the provided
     * Player object. The player that will be updated in the database, is the one whose ID
     * matches with the provided ID as parameter.
     *
     * @param player The Player object containing the new values
     * @param id     The ID of the player in the database to attribute the new values to.
     * @return True if the update was succesful
     * False if no rows were updated.
     */
    public boolean updatePlayer(Player player, int id) {
        String birthDate = new SimpleDateFormat("yyyy-MM-dd").format(player.getBirthDate());
        String country = player.getCountry();
        String name = player.getName();
        int number = player.getNumber();
        String position = player.getPosition();
        int team = player.getTeam().getId();

        String query = "UPDATE players SET name = ?, country = ?, position = ?, birth_date = ?, number = ?, team_id = ? WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setString(1, name);
            s.setString(2, country);
            s.setString(3, position);
            s.setString(4, birthDate);
            s.setInt(5, number);
            s.setInt(6, team);
            s.setInt(7, id);

            if (s.executeUpdate() > 0) {
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Deletes the player with the provided ID from the database.
     *
     * @param id The ID of the player to delete.
     * @return True if the player was succesfully deleted
     * False if there was nothing to delete.s
     */
    public boolean deletePlayer(int id) {
        String query = "DELETE FROM players WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, id);

            if (s.executeUpdate() > 0) {
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Creates an Object based on the provided resultset.
     *
     * @param rs The ResultSet containing the data for the object to create.
     * @return An object based on the provided resultset.
     */
    @Override
    public Object sqlToObject(ResultSet rs) {
        Player object = new Player();
        try {
            object.setId(rs.getInt("p.id"));
            object.setBirthDate(rs.getDate("p.birth_date"));
            object.setCountry(rs.getString("p.country"));
            object.setName(rs.getString("p.name"));
            object.setNumber(rs.getInt("p.number"));
            object.setPosition(rs.getString("p.position"));

            if (rs.getInt("team_id") != 0) {
                Team team = new Team();
                team.setId(rs.getInt("t.id"));
                team.setCountry(rs.getString("t.country"));
                team.setDivision(rs.getString("t.division"));
                team.setName(rs.getString("t.name"));
                object.setTeam(team);
            } else {
                object.setTeam(null);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return object;
    }
}
