package repository;

import domain.MatchEvent;
import domain.Player;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import services.PlayerService;
import utilities.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MatchEventDAO implements Convertable {

    private Connection con;

    public MatchEventDAO(){
        try {
            this.con = DriverManager.getConnection(ConnectionManager.URL, ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected  MatchEventDAO(Connection con){
        this.con = con;
    }

    /**
     * Searches the database for all match events belonging to the given match.
     * @param matchId The ID of the match
     * @return An ArrayList of MatchEvent objects
     */
    public List<MatchEvent> getMatchEventsFromMatch(int matchId){
        List<MatchEvent> events = new ArrayList<>();

        String query = "SELECT * FROM match_events me" +
                "       LEFT JOIN players p ON me.main_player = p.id " +
                "       WHERE me.match_id = ? " +
                "       ORDER BY me.id DESC";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, matchId);
            s.execute();

            ResultSet rs = s.getResultSet();

            while(rs.next()){
                MatchEvent event = (MatchEvent)sqlToObject(rs);
                events.add(event);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return events;
    }

    /**
     * Fetches a MatchEvent from the database corresponding to the given ID.
     * @param id The ID of the MatchEvent to find.
     * @return A MatchEvent object.
     */
    public MatchEvent getMatchEventById(int id){
        MatchEvent event = null;
        String query = "SELECT * FROM match_events me" +
                "       LEFT JOIN players p ON me.main_player = p.id " +
                "       WHERE me.id = ? " ;

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, id);
            s.execute();

            ResultSet rs = s.getResultSet();

            if(rs.next()){
                event = (MatchEvent)sqlToObject(rs);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return event;
    }
    /**
     * Adds a new match event.
     * @param event The MatchEvent object containing the values to be inserted
     * @param matchId The ID of match this event belongs to
     * @return True if the event was added, false if no rows were added.
     */
    public boolean addMatchEvent(MatchEvent event, int matchId){
        int playerId = event.getMainPlayer().getId();
        int time = event.getTime();
        int extraTime = event.getExtraTime();
        String type = event.getType();
        String side = event.getSide();

        String query = "INSERT INTO match_events (match_id, time, extra_time, type, main_player, side)" +
                "                   VALUES(?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, matchId);
            s.setInt(2, time);
            s.setInt(3, extraTime);
            s.setString(4, type);
            s.setInt(5, playerId);
            s.setString(6, side);

            if(s.executeUpdate() > 0){
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Edits the match and updates the values to those in the MatchEvent object.
     * @param event The MatchEvent object containing the new values
     * @param eventId The ID of the MatchEvent to be updated
     * @return True if the MatchEvent was updated, false if no rows were updated.
     */
    public boolean editMatchEvent(MatchEvent event, int eventId){
        int playerId = event.getMainPlayer().getId();
        int time = event.getTime();
        int extraTime = event.getExtraTime();
        String type = event.getType();
        String side = event.getSide();

        String query = "UPDATE match_events SET time = ?, extra_time = ?, type = ?, main_player = ?, side = ? WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, time);
            s.setInt(2, extraTime);
            s.setString(3, type);
            s.setInt(4, playerId);
            s.setString(5, side);
            s.setInt(6, eventId);

            if(s.executeUpdate() > 0){
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Deletes the MatchEvent with the given ID from the match_events table in the database.
     * @param eventId The ID of the MatchEvent to be deleted.
     * @return True if the MatchEvent was deleted, false if no rows were deleted.
     */
    public boolean deleteMatchEvent(int eventId){
        String query = "DELETE FROM match_events WHERE id = ?";

        try {
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, eventId);

            if(s.executeUpdate() > 0){
                s.close();
                return true;
            } else {
                s.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Creates an Object based on the provided resultset.
     *
     * @param rs The ResultSet containing the data for the object to create.
     * @return An object based on the provided resultset.
     */
    @Override
    public Object sqlToObject(ResultSet rs) {
        MatchEvent event = new MatchEvent();

        try {
            event.setId(rs.getInt("me.id"));
            event.setTime(rs.getInt("time"));
            event.setExtraTime(rs.getInt("extra_time"));
            event.setSide(rs.getString("side"));
            event.setType(rs.getString("type"));

            if(rs.getInt("main_player") > 0){
                Player player = new Player();
                player.setTeam(null);
                player.setPosition(rs.getString("p.position"));
                player.setNumber(rs.getInt("number"));
                player.setName(rs.getString("name"));
                player.setCountry(rs.getString("country"));
                player.setBirthDate(rs.getDate("birth_date"));
                event.setMainPlayer(player);
            } else {
                event.setMainPlayer(null);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return event;
    }
}
