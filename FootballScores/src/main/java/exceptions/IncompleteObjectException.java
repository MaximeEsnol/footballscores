package exceptions;

public class IncompleteObjectException extends Exception {
    public IncompleteObjectException(){
        super("The provided object is missing the minimum required data to be able to function properly.");
    }
}
