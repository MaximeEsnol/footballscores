package exceptions;

public class InvalidIDException extends Exception {
    public InvalidIDException(){
        super("The given ID is invalid.");
    }
}
