package exceptions;

public class NoRecordsFoundException extends Exception {
    public NoRecordsFoundException(){
        super("No records could be found.");
    }
}
