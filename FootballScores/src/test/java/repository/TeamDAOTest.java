package repository;

import domain.Team;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TeamDAOTest {
    private Connection con;


    @BeforeEach
    public void setup(){

        try {
            con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void close(){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getTeamById(){
        TeamDAO dao = new TeamDAO(con);
        Team team = dao.getTeamById(1);
        assertTrue(team != null);
    }


    private void setupDB(){
        try {
            Connection con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
            PreparedStatement s = con.prepareStatement("INSERT INTO teams (id, name, country, division)" +
                    "                                   VALUES(1, \"RSC Anderlecht\", \"Belgium\", \"1A\")");
            s.executeUpdate();
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
