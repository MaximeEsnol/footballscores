package repository;

import domain.Match;
import domain.Team;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MatchDAOTest {
    private Connection con;


    @BeforeEach
    public void setup(){

        try {
            con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void close(){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetAllMatches(){
        MatchDAO dao = new MatchDAO(con);
        List<Match> matches = new ArrayList<>();
        matches = dao.getAllMatches();

        assertTrue(!matches.isEmpty());
    }

    @Test
    public void testGetLatestMatches(){
        MatchDAO dao = new MatchDAO(con);
        List<Match> matches = new ArrayList<>();
        matches = dao.getLatestMatches(5);

        assertTrue(matches.size() <= 5);
    }

    @Test
    public void testGetMatchByID(){
        try {
            Connection con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
            PreparedStatement s = con.prepareStatement("INSERT INTO matches (id, home_team, away_team, division, match_day, home_team_score, away_team_score)" +
                    "                                   VALUES(1, 1, 3, \"1A\", 1, 1, 0)");
            s.executeUpdate();
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        MatchDAO dao = new MatchDAO(con);
        Match match = dao.getMatchById(1);

        assertTrue(match != null);
    }

    @Test
    public void testAddMatch(){
        MatchDAO dao = new MatchDAO(con);
        Match mock = createMockMatch();

        assertTrue(dao.addMatch(mock));
    }

    @Test
    public void testUpdateMatch(){
        try {
            Connection con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
            PreparedStatement s = con.prepareStatement("INSERT INTO matches (id, home_team, away_team, division, match_day, home_team_score, away_team_score)" +
                    "                                   VALUES(1, 1, 3, \"1A\", 1, 1, 0)");
            s.executeUpdate();
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        MatchDAO dao = new MatchDAO(con);
        Match mock = createMockMatch();

        assertTrue(dao.editMatch(mock, 1));
    }

    @Test
    public void testUpdateMatchScore(){
        try {
            Connection con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
            PreparedStatement s = con.prepareStatement("INSERT INTO matches (id, home_team, away_team, division, match_day, home_team_score, away_team_score)" +
                    "                                   VALUES(1, 1, 3, \"1A\", 1, 1, 0)");
            s.executeUpdate();
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        MatchDAO dao = new MatchDAO(con);

        assertTrue(dao.updateMatchScore(5,0,1));
    }

    @Test
    public void testDeleteMatch(){
        try {
            Connection con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
            PreparedStatement s = con.prepareStatement("INSERT INTO matches (id, home_team, away_team, division, match_day, home_team_score, away_team_score)" +
                    "                                   VALUES(1, 1, 3, \"1A\", 1, 1, 0)");
            s.executeUpdate();
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        MatchDAO dao = new MatchDAO(con);

        assertTrue(dao.deleteMatch(1));
    }

    private Match createMockMatch(){
        Match m = new Match();
        m.setHomeTeamScore(2);
        m.setAwayTeamScore(2);
        m.setDivision("1A");
        m.setMatchDay(5);
        Team h = new Team();
        h.setId(1);
        Team a = new Team();
        a.setId(3);
        m.setHomeTeam(h);
        m.setAwayTeam(a);

        return m;
    }
}
