package repository;

import domain.Player;
import domain.Team;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PlayerDAOTest {
    private Connection con;

    @BeforeEach
    public void setup() {
        try {
            con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void close() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getPlayerById(){
        PlayerDAO dao = new PlayerDAO(con);

        Player p = dao.getPlayerById(2);
        assertTrue(p != null);
    }

    @Test
    public void getPlayersWithMostGoals(){
        PlayerDAO dao = new PlayerDAO(con);

        List<Player> players = dao.getPlayersWithMostGoals();

        assertAll(() -> {
            assertTrue(!players.isEmpty());
            assertTrue(players.size() <= 5);
        });
    }

    @Test
    public void getPlayers(){
        PlayerDAO dao = new PlayerDAO(con);
        List<Player> players = dao.getPlayers();

        assertTrue(!players.isEmpty());
    }

    @Test
    public void getPlayersFromTeam(){
        PlayerDAO dao = new PlayerDAO(con);
        List<Player> players = dao.getPlayersFromTeam("RSC Anderlecht");

        assertTrue(!players.isEmpty());
    }

    @Test
    public void addPlayer(){
        setupDB();
        PlayerDAO dao = new PlayerDAO(con);

        assertTrue(dao.addPlayer(createFakePlayer()));
    }

    @Test
    public void editPlayer(){
        setupDB();
        PlayerDAO dao = new PlayerDAO(con);

        assertTrue(dao.updatePlayer(createFakePlayer(), 1));
    }

    @Test
    public void deletePlayer(){
        setupDB();
        PlayerDAO dao = new PlayerDAO(con);

        assertTrue(dao.deletePlayer(1));
    }

    private Player createFakePlayer(){
        Player p = new Player();

        p.setPosition("Midfielder");
        p.setCountry("Belgium");
        p.setName("John Doe");
        p.setNumber(1);
        p.setBirthDate(new Date(100, 1, 1));
        Team t = new Team();
        t.setId(1);
        p.setTeam(t);

        return p;
    }

    private void setupDB(){
        try {
            Connection con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
            PreparedStatement s = con.prepareStatement("INSERT INTO players (id, name, team_id, birth_date, position, country, number)" +
                    "                                   VALUES(1, \"John Doe\", 1, \"2000-01-01\", \"Forward\", \"Belgium\", 1)");
            s.executeUpdate();
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
