package repository;

import domain.MatchEvent;
import domain.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MatchEventDAOTest {
    private Connection con;

    @BeforeEach
    public void setup(){

        try {
            con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void close(){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetMatchEventsFromMatch(){
        MatchEventDAO dao = new MatchEventDAO(con);

        List<MatchEvent> e = dao.getMatchEventsFromMatch(1);
        assertTrue(!e.isEmpty());
    }

    @Test
    public void testGetMatchEventById(){
        MatchEventDAO dao = new MatchEventDAO(con);

        MatchEvent e = dao.getMatchEventById(1);
        assertTrue(e != null);
    }

    @Test
    public void testAddMatchEvent(){
        setupDB();
        MatchEventDAO dao = new MatchEventDAO(con);

        MatchEvent e = createFakeEvent();
        assertTrue(dao.addMatchEvent(e, 3));
    }

    @Test
    public void testEditMatchEvent(){
        setupDB();
        MatchEventDAO dao = new MatchEventDAO(con);

        MatchEvent e = createFakeEvent();
        assertTrue(dao.editMatchEvent(e, 1));
    }

    @Test
    public void testDeleteMatchEvent(){
        setupDB();
        MatchEventDAO dao = new MatchEventDAO(con);

        assertTrue(dao.deleteMatchEvent(1));
    }

    private MatchEvent createFakeEvent(){
        MatchEvent e = new MatchEvent();
        e.setSide("HOME_TEAM");
        e.setType("GOAL");
        e.setTime(40);
        e.setExtraTime(0);
        Player p = new Player();
        p.setId(1);
        e.setMainPlayer(p);
        return e;
    }

    private void setupDB(){
        try {
            Connection con = DriverManager.getConnection(ConnectionManager.URL + "test", ConnectionManager.USERNAME, ConnectionManager.PASSWORD);
            PreparedStatement s = con.prepareStatement("INSERT INTO match_events (id, match_id, time, extra_time, type, main_player, side)" +
                    "                                   VALUES(1, 2, 10, 0, \"GOAL\", 2, \"HOME_TEAM\")");
            s.executeUpdate();
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
