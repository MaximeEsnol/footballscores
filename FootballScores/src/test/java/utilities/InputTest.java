package utilities;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Date;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InputTest {

    @Test
    public void getInt(){
        System.setIn(getTestInputStream("10"));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        final int expectedOutput = 10;

        int actualOutput = input.getInt(false);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void getIntFailed(){
        System.setIn(getTestInputStream("not an integer"));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        final int expected = 0;
        int actual = input.getInt(false);

        assertEquals(expected, actual);
    }

    @Test
    public void getDouble(){
        System.setIn(getTestInputStream("1.00"));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        final double expectedOutput = 1.00;

        double actualOutput = input.getDouble(false);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void getDoubleFailed(){
        System.setIn(getTestInputStream("not a double"));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        final double expected = 0;
        double actual = input.getDouble(false);

        assertEquals(expected, actual);
    }

    @Test
    public void getString(){
        System.setIn(getTestInputStream("Hello world"));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        final String expectedOutput = "Hello world";

        String actualOutput = input.getString(false);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void getStringFailed(){
        System.setIn(getTestInputStream(""));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        final String expected = null;
        String actual = input.getString(false);

        assertEquals(expected, actual);
    }

    @Test
    public void getDate(){
        System.setIn(getTestInputStream("1999-02-11"));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        Date expectedOutput = new Date(99, 1, 11);

        Date actualOutput = input.getDate(false);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void getDateFailed(){
        System.setIn(getTestInputStream("not a date "));
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        Date expected = null;
        Date actual = input.getDate(false);

        assertEquals(expected, actual);
    }

    private InputStream getTestInputStream(String input){
        InputStream in = new ByteArrayInputStream(input.getBytes());
        return in;
    }
}
