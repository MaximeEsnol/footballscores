package utilities;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.DateTimeException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class DateUtilsTest {

    @Test
    public void getAge(){
        final int expected = 21;
        final int actual = DateUtils.getAge(new Date(97, 10, 31));

        assertEquals(expected, actual);
    }

    @Test
    public void getAgeDateTimeException(){
        assertThrows(DateTimeException.class, () -> {
           DateUtils.getAge(new Date(119, 11, 1));
        });
    }
}
