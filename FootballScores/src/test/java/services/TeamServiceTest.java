package services;

import domain.Team;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.TeamDAO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TeamServiceTest {
    @Mock
    private TeamDAO dao;

    @InjectMocks
    TeamService service;



    @Test
    public void testGetTeamById() throws InvalidIDException, NoRecordsFoundException {
        Team team = new Team();
        when(dao.getTeamById(1)).thenReturn(team);
        Team result = service.getTeamById(1);
        assertEquals(team, result);
    }

    @Test
    public void getTeamIdInvalidIDException(){
        assertThrows(InvalidIDException.class, () -> {
           service.getTeamById(-1);
        });
    }

    @Test
    public void getTeamIdNoRecordsException(){
        assertThrows(NoRecordsFoundException.class, () -> {
           when(dao.getTeamById(250)).thenReturn(null);
           service.getTeamById(250);
        });
    }
}
