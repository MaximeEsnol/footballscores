package services;

import domain.Player;
import domain.Team;
import exceptions.IncompleteObjectException;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.PlayerDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PlayerServiceTest {

    @Mock
    private PlayerDAO dao;

    @InjectMocks
    PlayerService service;

    @Test
    public void testGetPlayerById() throws InvalidIDException, NoRecordsFoundException {
        Player player = new Player();
        when(dao.getPlayerById(1)).thenReturn(player);
        Player result = service.getPlayerById(1);
        assertEquals(player, result);
    }

    @Test
    public void getGameByIdInvalidIDException(){
        Assertions.assertThrows(InvalidIDException.class, () -> {
            service.getPlayerById(-1);
        });
    }

    @Test
    public void getGameByIdNoRecordsFoundException(){
        Assertions.assertThrows(NoRecordsFoundException.class, () -> {
           when(dao.getPlayerById(50000)).thenReturn(null);
           service.getPlayerById(50000);
        });
    }

    @Test
    public void getPlayersWithMostGoals() throws NoRecordsFoundException {
        List<Player> players = new ArrayList<>();
        players.add(new Player());
        when(dao.getPlayersWithMostGoals()).thenReturn(players);
        List<Player> results = service.getPlayersWithMostGoals();
        Assertions.assertEquals(results, players);
    }

    @Test
    public void getPlayersMostGoalsNoRecordFoundException(){
        Assertions.assertThrows(NoRecordsFoundException.class, () -> {
            List<Player> players = new ArrayList<>();
            when(dao.getPlayersWithMostGoals()).thenReturn(players);
            service.getPlayersWithMostGoals();
        });
    }

    @Test
    public void getAllPlayers() throws NoRecordsFoundException {
        List<Player> players = new ArrayList<>();
        players.add(new Player());
        when(dao.getPlayers()).thenReturn(players);
        List<Player> results = service.getPlayers();
        assertEquals(players, results);
    }

    @Test
    public void getAllPlayersNoRecords(){
        assertThrows(NoRecordsFoundException.class, () -> {
            List<Player> players = new ArrayList<>();
            when(dao.getPlayers()).thenReturn(players);
            service.getPlayers();
        });
    }

    @Test
    public void getPlayersFromTeam() throws NoRecordsFoundException {
        List<Player> players = new ArrayList<>();
        players.add(new Player());
        when(dao.getPlayersFromTeam("Anderlecht")).thenReturn(players);
        List<Player> results = service.getPlayersFromTeam("Anderlecht");
        assertEquals(results, players);
    }

    @Test
    public void getPlayersFromTeamIllegalArg(){
        assertThrows(IllegalArgumentException.class, () -> {
            service.getPlayersFromTeam("");
        });
    }

    @Test
    public void getPlayersFromTeamNoRecords(){
        assertThrows(NoRecordsFoundException.class, () -> {
            List<Player> players = new ArrayList<>();
            when(dao.getPlayersFromTeam("KV Mechelen")).thenReturn(players);
            service.getPlayersFromTeam("KV Mechelen");
        });
    }

    @Test
    public void addPlayer() throws IncompleteObjectException {
        Player p = createFakePlayer();

        when(dao.addPlayer(p)).thenReturn(true);
        assertTrue(service.addPlayer(p));
    }

    @Test
    public void addPlayerFailed() throws IncompleteObjectException {
        Player p = createFakePlayer();
        when(dao.addPlayer(p)).thenReturn(false);
        assertFalse(service.addPlayer(p));
    }

    @Test
    public void addPlayerIncompleteObject(){
        Player p = new Player();
        assertThrows(IncompleteObjectException.class, () -> {
            service.addPlayer(p);
        });
    }

    @Test
    public void editPlayer() throws IncompleteObjectException, InvalidIDException {
        Player p = createFakePlayer();
        when(dao.updatePlayer(p, 1)).thenReturn(true);
        assertTrue(service.updatePlayer(p, 1));
    }

    @Test
    public void editPlayerFailed() throws IncompleteObjectException, InvalidIDException {
        Player p = createFakePlayer();
        when(dao.updatePlayer(p, 100)).thenReturn(false);
        assertFalse(service.updatePlayer(p, 100));
    }

    @Test
    public void editPlayerIncompleteObject(){
        Player p = new Player();
        assertThrows(IncompleteObjectException.class, () -> {
            service.updatePlayer(p, 100);
        });
    }

    @Test
    public void editPlayerInvalidID(){
        Player p = createFakePlayer();
        assertThrows(InvalidIDException.class, () -> {
            service.updatePlayer(p, 0);
        });
    }

    @Test
    public void deletePlayer() throws InvalidIDException {
        when(dao.deletePlayer(1)).thenReturn(true);
        assertTrue(service.deletePlayer(1));
    }

    @Test
    public void deletePlayerFailed() throws InvalidIDException {
        when(dao.deletePlayer(1000)).thenReturn(false);
        assertFalse(service.deletePlayer(1000));
    }

    @Test
    public void deletePlayerInvalidID(){
        assertThrows(InvalidIDException.class, () -> {
            service.deletePlayer(0);
        });
    }

    private static Player createFakePlayer(){
        Player p = new Player();
        p.setId(1);
        p.setBirthDate(new Date());
        p.setNumber(1);
        p.setName("John Doe");
        p.setCountry("Belgium");
        p.setPosition("Bench");
        Team team = new Team();
        p.setTeam(team);

        return p;
    }
}
