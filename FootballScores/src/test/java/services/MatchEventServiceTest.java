package services;

import domain.MatchEvent;
import domain.Player;
import exceptions.IncompleteObjectException;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.MatchEventDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MatchEventServiceTest {

    @Mock
    MatchEventDAO d;

    @InjectMocks
    MatchEventService s;

    @Test
    public void getMatchEventsFromMatch() throws InvalidIDException, NoRecordsFoundException {
        List<MatchEvent> events = new ArrayList<>();
        events.add(new MatchEvent());
        when(d.getMatchEventsFromMatch(1)).thenReturn(events);
        List<MatchEvent> results = s.getMatchEventsFromMatch(1);
        assertEquals(results, events);
    }

    @Test
    public void getMatchEventsFromMatchInvalidID(){
        assertThrows(InvalidIDException.class, () -> {
            s.getMatchEventsFromMatch(0);
        });
    }

    @Test
    public void getMatchEventsFromMatchNoRecords(){
        assertThrows(NoRecordsFoundException.class, () -> {
            List<MatchEvent> events = new ArrayList<>();
            when(d.getMatchEventsFromMatch(500)).thenReturn(events);
            s.getMatchEventsFromMatch(500);
        });
    }

    @Test
    public void getMatchEventById() throws InvalidIDException, NoRecordsFoundException {
        MatchEvent event = new MatchEvent();
        when(d.getMatchEventById(1)).thenReturn(event);
        MatchEvent result = s.getMatchEventById(1);
        assertEquals(result, event);
    }

    @Test
    public void getMatchEventByIdNoRecords(){
        assertThrows(NoRecordsFoundException.class, () -> {
            when(d.getMatchEventById(100)).thenReturn(null);
            s.getMatchEventById(100);
        });
    }

    @Test
    public void getMatchEventByIdInvalidID(){
        assertThrows(InvalidIDException.class, () -> {
            s.getMatchEventById(0);
        });
    }

    @Test
    public void addMatchEvent() throws IncompleteObjectException, InvalidIDException {
        MatchEvent e = createFakeEvent();

        when(d.addMatchEvent(e, 1)).thenReturn(true);
        assertTrue(s.addMatchEvent(e, 1));
        verify(d, times(1)).addMatchEvent(anyObject(), anyInt());
    }

    @Test
    public void addMatchEventAdditionFailed() throws IncompleteObjectException, InvalidIDException {
        MatchEvent e = createFakeEvent();
        when(d.addMatchEvent(e, 1000)).thenReturn(false);
        assertFalse(s.addMatchEvent(e, 1000));
    }

    @Test
    public void addMatchEventIncompleteObject(){
        MatchEvent e = null;

        assertThrows(IncompleteObjectException.class, () -> {
            s.addMatchEvent(e, 1);
        });
    }

    @Test
    public void addMatchEventInvalidMatchId(){
        MatchEvent e = createFakeEvent();
        assertThrows(InvalidIDException.class, () -> {
            s.addMatchEvent(e, 0);
        });
    }

    @Test
    public void editMatchEvent() throws IncompleteObjectException, InvalidIDException {
        MatchEvent e = createFakeEvent();
        when(d.editMatchEvent(e, 1)).thenReturn(true);

        assertTrue(s.editMatchEvent(e, 1));
    }

    @Test
    public void editMatchEventEditionFailed() throws IncompleteObjectException, InvalidIDException {
        MatchEvent e = createFakeEvent();
        when(d.editMatchEvent(e, 1000)).thenReturn(false);
        assertFalse(s.editMatchEvent(e, 1000));
    }

    @Test
    public void editMatchEventIncompleteObject(){
        MatchEvent e = null;
        assertThrows(IncompleteObjectException.class, () -> {
            s.editMatchEvent(e, 1);
        });
    }

    @Test
    public void editMatchEventInvalidID(){
        MatchEvent e = createFakeEvent();
        assertThrows(InvalidIDException.class, () -> {
            s.editMatchEvent(e, 0);
        });
    }

    @Test
    public void deleteMatchEventTest() throws InvalidIDException {
        when(d.deleteMatchEvent(1)).thenReturn(true);
        assertTrue(s.deleteMatchEvent(1));
    }

    @Test
    public void deleteMatchEventInvalidID(){
        assertThrows(InvalidIDException.class, () -> {
            s.deleteMatchEvent(0);
        });
    }

    @Test
    public void deleteMatchEventDeletionFailed() throws InvalidIDException {
        when(d.deleteMatchEvent(5000)).thenReturn(false);
        assertFalse(s.deleteMatchEvent(5000));
    }

    private static MatchEvent createFakeEvent(){
        MatchEvent e = new MatchEvent();
        e.setSide("HOME_TEAM");
        e.setType("GOAL");
        e.setTime(40);
        e.setExtraTime(0);
        Player p = new Player();
        p.setId(1);
        e.setMainPlayer(p);
        return e;
    }
}
