package services;

import domain.Match;
import domain.Player;
import domain.Team;
import exceptions.IncompleteObjectException;
import exceptions.InvalidIDException;
import exceptions.NoRecordsFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.MatchDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MatchServiceTest {

    @Mock
    MatchDAO dao;

    @InjectMocks
    MatchService service;

    @Test
    public void getAllMatches() throws NoRecordsFoundException {
        List<Match> matches = new ArrayList<>();
        matches.add(new Match());
        when(dao.getAllMatches()).thenReturn(matches);
        List<Match> results = service.getAllMatches();
        assertEquals(results, matches);
    }

    @Test
    public void getAllMatchesNoRecord(){
        assertThrows(NoRecordsFoundException.class, () -> {
            List<Match> matches = new ArrayList<>();
            when(dao.getAllMatches()).thenReturn(matches);
            service.getAllMatches();
        });
    }

    @Test
    public void getMatchById() throws InvalidIDException, NoRecordsFoundException {
        Match match = new Match();
        when(dao.getMatchById(1)).thenReturn(match);
        Match result = service.getMatchById(1);
        assertEquals(result, match);
    }

    @Test
    public void getMatchByIdInvalidId(){
        assertThrows(InvalidIDException.class, () -> {
            service.getMatchById(0);
        });
    }

    @Test
    public void getMatchByIdNoRecord(){
        assertThrows(NoRecordsFoundException.class, () -> {
            when(dao.getMatchById(5000)).thenReturn(null);
            service.getMatchById(5000);
        });
    }

    @Test
    public void getLatestMatches() throws NoRecordsFoundException {
        List<Match> matches = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            matches.add(new Match());
        }
        when(dao.getLatestMatches(5)).thenReturn(matches);
        List<Match> results = service.getLatestMatches(5);
        assertAll(() -> {
            assertEquals(results, matches);
            assertEquals(results.size(), matches.size());
        });
    }

    @Test
    public void getLatestMatchesIllegalArg(){
        assertThrows(IllegalArgumentException.class, () -> {
            service.getLatestMatches(-4);
        });
    }

    @Test
    public void getLatestMatchesNoRecord(){
        assertThrows(NoRecordsFoundException.class, () -> {
            List<Match> matches = new ArrayList<>();
            when(dao.getLatestMatches(5)).thenReturn(matches);
            service.getLatestMatches(5);
        });
    }

    @Test
    public void addMatch() throws IncompleteObjectException {
        Match m = createFakeMatch();
        when(dao.addMatch(m)).thenReturn(true);
        assertTrue(service.addMatch(m));
    }

    @Test
    public void addMatchFailed() throws IncompleteObjectException {
        Match m = createFakeMatch();
        when(dao.addMatch(m)).thenReturn(false);
        assertFalse(service.addMatch(m));
    }

    @Test
    public void addMatchIncompleteObject(){
        Match m = new Match();
        assertThrows(IncompleteObjectException.class, () -> {
            service.addMatch(m);
        });
    }

    @Test
    public void editMatch() throws IncompleteObjectException, InvalidIDException {
        Match m = createFakeMatch();
        when(dao.editMatch(m, 1)).thenReturn(true);
        assertTrue(service.editMatch(m, 1));
    }

    @Test
    public void editMatchIncompleteObject(){
        Match m = new Match();
        assertThrows(IncompleteObjectException.class, () -> {
            service.editMatch(m, 1);
        });
    }

    @Test
    public void editMatchFailed() throws IncompleteObjectException, InvalidIDException {
        Match m = createFakeMatch();
        when(dao.editMatch(m, 1000)).thenReturn(false);
        assertFalse(service.editMatch(m, 1000));
    }

    @Test
    public void editMatchInvalidID(){
        Match m = createFakeMatch();
        assertThrows(InvalidIDException.class, () -> {
            service.editMatch(m, 0);
        });
    }

    @Test
    public void updateMatchScore() throws InvalidIDException {
        final int homeScore = 0;
        final int awayScore = 1;

        when(dao.updateMatchScore(homeScore, awayScore, 1)).thenReturn(true);
        assertTrue(service.updateMatchScore(homeScore, awayScore, 1));
    }

    @Test
    public void updateMatchScoreIllegalArg(){
        assertThrows(IllegalArgumentException.class, () -> {
           service.updateMatchScore(-1, 0, 1);
           service.updateMatchScore(-1, -1, 1);
           service.updateMatchScore(0, -1, 1);
        });
    }

    @Test
    public void updateMatchScoreInvalidID(){
        assertThrows(InvalidIDException.class, () -> {
            service.updateMatchScore(1, 2, 0);
        });
    }

    @Test
    public void updateMatchScoreFailed() throws InvalidIDException {
        when(dao.updateMatchScore(0, 0, 500)).thenReturn(false);
        assertFalse(service.updateMatchScore(0, 0, 500));
    }

    @Test
    public void deleteMatch() throws InvalidIDException {
        when(dao.deleteMatch(1)).thenReturn(true);
        assertTrue(service.deleteMatch(1));
    }

    @Test
    public void deleteMatchFailed() throws InvalidIDException {
        when(dao.deleteMatch(500)).thenReturn(false);
        assertFalse(service.deleteMatch(500));
    }

    @Test
    public void deleteMatchInvalidID(){
        assertThrows(InvalidIDException.class, () -> {
            service.deleteMatch(0);
        });
    }

    private static Match createFakeMatch(){
        Match m = new Match();
        m.setDivision("1A");
        m.setMatchDay(1);
        m.setHomeTeamScore(1);
        m.setAwayTeamScore(2);
        Team h = new Team();
        h.setId(1);
        Team a = new Team();
        a.setId(2);
        m.setHomeTeam(h);
        m.setAwayTeam(a);

        return m;
    }
}
