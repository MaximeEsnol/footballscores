-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: footballscores
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `match_events`
--

DROP TABLE IF EXISTS `match_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_id` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `extra_time` int(11) DEFAULT NULL,
  `type` enum('GOAL','OWN_GOAL','YELLOW_CARD','RED_CARD','YELLOWRED_CARD','SUBSTITUTION','PENALTY_MISSED','PENALTY_SCORED','GOAL_CANCELLED') DEFAULT NULL,
  `main_player` int(11) NOT NULL,
  `side` enum('HOME_TEAM','AWAY_TEAM') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_events`
--

/*!40000 ALTER TABLE `match_events` DISABLE KEYS */;
INSERT INTO `match_events` VALUES (1,1,10,0,'GOAL',1,'HOME_TEAM'),(2,1,15,0,'GOAL',2,'HOME_TEAM'),(3,1,45,3,'GOAL',19,'AWAY_TEAM');
/*!40000 ALTER TABLE `match_events` ENABLE KEYS */;

--
-- Table structure for table `matches`
--

DROP TABLE IF EXISTS `matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `home_team` int(11) DEFAULT NULL,
  `away_team` int(11) DEFAULT NULL,
  `division` varchar(98) DEFAULT NULL,
  `match_day` int(11) DEFAULT NULL,
  `home_team_score` int(11) NOT NULL DEFAULT '0',
  `away_team_score` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matches`
--

/*!40000 ALTER TABLE `matches` DISABLE KEYS */;
INSERT INTO `matches` VALUES (1,1,2,'1A',1,2,1),(2,1,2,'PlayOffs',1,2,2);
/*!40000 ALTER TABLE `matches` ENABLE KEYS */;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `position` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (1,'Nany Dimata',1,'1997-09-01','Forward','Belgium',9),(2,'Kemar Roofe',1,'1993-01-06','Forward','Jamaica',7),(3,'Albert Sambi Lokonga',1,'1999-10-22','Defensive Midfielder','Belgium',48),(4,'Jeremy Doku',1,'2002-05-27','Forward','Belgium',49),(5,'Francis Amuzu',1,'1999-08-23','Forward','Belgium',40),(6,'Yari Verschaeren',1,'2001-07-12','Attacking Midfielder','Belgium',51),(7,'Nacer Chadli',1,'1989-08-02','Forward','Belgium',22),(8,'Michel Vlap',1,'1997-06-02','Midfielder','Netherlands',10),(9,'Adrien Trebel',1,'1991-03-03','Midfielder','France',28),(10,'Pieter Gerkens',1,'1995-08-13','Midfielder','Belgium',24),(11,'Peter Zulj',1,'1993-06-09','Defensive Midfielder','Austria',26),(12,'Alexis Saelemaekers',1,'1999-06-27','Winger','Belgium',20),(13,'Vincent Kompany',1,'1986-04-10','Defender','Belgium',4),(14,'Philippe Sandler',1,'1997-02-10','Defender','Netherlands',22),(15,'Derrick Luckassen',1,'1995-07-03','Defender','Netherlands',23),(16,'Elias Cobbaut',1,'1997-11-04','Defender','Belgium',22),(17,'Hendrik Van Crombrugge',1,'1993-04-30','Goalkeeper','Belgium',30),(18,'Thomas Didillon',1,'1995-11-28','Goalkeeper','France',16),(19,'Hans Vanaken',2,'1992-08-24','Midfielder','Belgium',20),(20,'Krépin Diatta',2,'1999-02-25','Winger','Senegal',11),(21,'Siebe Schrijvers',2,'1996-07-18','Attacking Midfielder','Belgium',16);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `division` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'RSC Anderlecht','Belgium','1A'),(2,'Club Brugge','Belgium','1A'),(3,'Standard Liege','Belgium','1A'),(4,'AA Gent','Belgium','1A'),(5,'Royal Antwerp FC','Belgium','1A'),(6,'KRC Genk','Belgium','1A');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-14 15:09:31
