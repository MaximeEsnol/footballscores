# FootballScores 
FootballScores is a program that allows you to see the latest match information and information about football scores.

## How to install

### Step 1: Clone the repository
Open your IDE and clone the repository as a new project. 
Once the project is loaded, open the 'Maven' menu and perform a clean install by selecting 'Clean' and 'Install' in the lifecycle menu.

### Step 2: Install the database
Start MySQL and copy the contents of the footballscores.sql file into an SQL editor. Run the queries. This will create the database and insert some rows in the tables.

### Step 3: Enjoy FootballScores :)